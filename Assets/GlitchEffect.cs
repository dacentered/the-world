/**
This work is licensed under a Creative Commons Attribution 3.0 Unported License.
http://creativecommons.org/licenses/by/3.0/deed.en_GB

You are free:

to copy, distribute, display, and perform the work
to make derivative works
to make commercial use of the work
*/

using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("Image Effects/GlitchEffect")]
[RequireComponent(typeof(Camera))]
public class GlitchEffect : MonoBehaviour
{
    private float _flickerTime = 0.5f;
    private float _glitchdown;
    private float _glitchdownTime = 0.05f;

    private float _glitchup;
    private float _glitchupTime = 0.05f;
    private Material _material;

    [Range(0, 1)] public float colorIntensity;

    public Texture2D displacementMap;
    private float flicker;

    [Range(0, 1)] public float flipIntensity;

    [Header("Glitch Intensity")] [Range(0, 1)]
    public float intensity;

    public Shader Shader;

    private void Start() => this._material = new Material(this.Shader);

    // Called by camera to apply image effect
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        this._material.SetFloat("_Intensity", this.intensity);
        this._material.SetFloat("_ColorIntensity", this.colorIntensity);
        this._material.SetTexture("_DispTex", this.displacementMap);

        this.flicker += Time.deltaTime * this.colorIntensity;
        if (this.flicker > this._flickerTime)
        {
            this._material.SetFloat("filterRadius", Random.Range(-3f, 3f) * this.colorIntensity);
            this._material.SetVector("direction",
                Quaternion.AngleAxis(Random.Range(0, 360) * this.colorIntensity, Vector3.forward) * Vector4.one);
            this.flicker = 0;
            this._flickerTime = Random.value;
        }

        if (this.colorIntensity == 0) this._material.SetFloat("filterRadius", 0);

        this._glitchup += Time.deltaTime * this.flipIntensity;
        if (this._glitchup > this._glitchupTime)
        {
            if (Random.value < 0.1f * this.flipIntensity)
                this._material.SetFloat("flip_up", Random.Range(0, 1f) * this.flipIntensity);
            else
                this._material.SetFloat("flip_up", 0);

            this._glitchup = 0;
            this._glitchupTime = Random.value / 10f;
        }

        if (this.flipIntensity == 0) this._material.SetFloat("flip_up", 0);

        this._glitchdown += Time.deltaTime * this.flipIntensity;
        if (this._glitchdown > this._glitchdownTime)
        {
            if (Random.value < 0.1f * this.flipIntensity)
                this._material.SetFloat("flip_down", 1 - Random.Range(0, 1f) * this.flipIntensity);
            else
                this._material.SetFloat("flip_down", 1);

            this._glitchdown = 0;
            this._glitchdownTime = Random.value / 10f;
        }

        if (this.flipIntensity == 0) this._material.SetFloat("flip_down", 1);

        if (Random.value < 0.05 * this.intensity)
        {
            this._material.SetFloat("displace", Random.value * this.intensity);
            this._material.SetFloat("scale", 1 - Random.value * this.intensity);
        }
        else
        {
            this._material.SetFloat("displace", 0);
        }

        Graphics.Blit(source, destination, this._material);
    }
}