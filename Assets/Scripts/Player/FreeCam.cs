using UnityEngine;

namespace Player
{
    /// <summary>
    ///     A simple free camera to be added to a Unity game object.
    ///     Keys:
    ///     wasd / arrows	- movement
    ///     q/e 			- up/down (local space)
    ///     r/f 			- up/down (world space)
    ///     pageup/pagedown	- up/down (world space)
    ///     hold shift		- enable fast movement mode
    ///     right mouse  	- enable free look
    ///     mouse			- free look / rotation
    /// </summary>
    public class FreeCam : MonoBehaviour
    {
        /// <summary>
        ///     Speed of camera movement when shift is held down,
        /// </summary>
        public float fastMovementSpeed = 100f;

        /// <summary>
        ///     Amount to zoom the camera when using the mouse wheel (fast mode).
        /// </summary>
        public float fastZoomSensitivity = 50f;

        /// <summary>
        ///     Sensitivity for free look.
        /// </summary>
        public float freeLookSensitivity = 3f;

        /// <summary>
        ///     Set to true when free looking (on right mouse button).
        /// </summary>
        private bool looking;

        /// <summary>
        ///     Normal speed of camera movement.
        /// </summary>
        public float movementSpeed = 10f;

        /// <summary>
        ///     Amount to zoom the camera when using the mouse wheel.
        /// </summary>
        public float zoomSensitivity = 10f;

        private void Update()
        {
            bool fastMode = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
            float movementSpeed = fastMode ? this.fastMovementSpeed : this.movementSpeed;

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                this.transform.position =
                    this.transform.position + -this.transform.right * movementSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                this.transform.position =
                    this.transform.position + this.transform.right * movementSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                this.transform.position =
                    this.transform.position + this.transform.forward * movementSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                this.transform.position =
                    this.transform.position + -this.transform.forward * movementSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.Q))
                this.transform.position = this.transform.position + this.transform.up * movementSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.E))
                this.transform.position = this.transform.position + -this.transform.up * movementSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.R) || Input.GetKey(KeyCode.PageUp))
                this.transform.position = this.transform.position + Vector3.up * movementSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.F) || Input.GetKey(KeyCode.PageDown))
                this.transform.position = this.transform.position + -Vector3.up * movementSpeed * Time.deltaTime;

            if (this.looking)
            {
                float newRotationX = this.transform.localEulerAngles.y +
                                     Input.GetAxis("Mouse X") * this.freeLookSensitivity;
                float newRotationY = this.transform.localEulerAngles.x -
                                     Input.GetAxis("Mouse Y") * this.freeLookSensitivity;
                this.transform.localEulerAngles = new Vector3(newRotationY, newRotationX, 0f);
            }

            float axis = Input.GetAxis("Mouse ScrollWheel");
            if (axis != 0)
            {
                float zoomSensitivity = fastMode ? this.fastZoomSensitivity : this.zoomSensitivity;
                this.transform.position += this.transform.forward * axis * zoomSensitivity;
            }

            if (Input.GetKeyDown(KeyCode.Mouse1))
                this.StartLooking();
            else if (Input.GetKeyUp(KeyCode.Mouse1)) this.StopLooking();
        }

        private void OnDisable() => this.StopLooking();

        /// <summary>
        ///     Enable free looking.
        /// </summary>
        public void StartLooking()
        {
            this.looking = true;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        /// <summary>
        ///     Disable free looking.
        /// </summary>
        public void StopLooking()
        {
            this.looking = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}