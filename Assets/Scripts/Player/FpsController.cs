﻿using UnityEngine;

public class FpsController : MonoBehaviour
{
    private readonly float jumpForce = 250.0f;
    private Transform cameraT;

    private bool cursorVisible;
    public bool grounded;

    public float mouseSensitivityX = 1.0f;
    public float mouseSensitivityY = 1.0f;
    private Vector3 moveAmount;

    private Vector3 smoothMoveVelocity;
    private float verticalLookRotation;

    public float walkSpeed = 10.0f;

    // Use this for initialization
    private void Start()
    {
        this.cameraT = Camera.main.transform;
        this.LockMouse();
    }

    // Update is called once per frame
    private void Update()
    {
        // rotation

        this.transform.Rotate(Input.GetAxis("Mouse X") * this.mouseSensitivityX * Vector3.up, Space.World);
        this.transform.Rotate(Input.GetAxis("Mouse Y") * this.mouseSensitivityY * Vector3.left);
        /*this.verticalLookRotation += Input.GetAxis("Mouse Y") * this.mouseSensitivityY;
        this.verticalLookRotation = Mathf.Clamp(this.verticalLookRotation, -60, 60);
        this.cameraT.localEulerAngles = Vector3.left * this.verticalLookRotation + Vector3.up* Input.GetAxis("Mouse X") * this.mouseSensitivityX;*/

        // movement
        Vector3 moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        Vector3 targetMoveAmount = moveDir * this.walkSpeed;
        this.moveAmount = targetMoveAmount;
        //this.moveAmount = Vector3.SmoothDamp(this.moveAmount, targetMoveAmount, ref this.smoothMoveVelocity, .15f);

        // jump
        /*if (Input.GetButtonDown("Jump"))
            if (this.grounded)
                this.rigidbodyR.AddForce(this.transform.up * this.jumpForce);*/
        World.Structure.World world = FindObjectOfType<World.Structure.World>();
        Vector3 pos = this.transform.position + 2.1f * Vector3.down;

        if (world.GetBlock(Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y), Mathf.FloorToInt(pos.z)).Id != 0)
            this.grounded = true;
        else
            this.grounded = false;


        if (this.grounded)
        {
            Vector3 translation = this.moveAmount * Time.deltaTime;
            Vector3 dir = translation;
            Debug.Log(dir);
            dir = dir.x * this.transform.right +
                  dir.z * new Vector3(this.transform.forward.x, 0, this.transform.forward.z);
            pos = this.transform.position + dir;
            Vector3 checkPos = this.transform.position + dir * 1.5f;
            Debug.DrawRay(this.transform.position, 50 * dir, Color.red, 1);
            float originalY = this.transform.position.y;
            if (world.GetBlock(Mathf.FloorToInt(checkPos.x), Mathf.FloorToInt(checkPos.y), Mathf.FloorToInt(checkPos.z))
                    .Id == 0 && world.GetBlock(Mathf.FloorToInt(checkPos.x), Mathf.FloorToInt(checkPos.y) - 1,
                    Mathf.FloorToInt(checkPos.z)).Id == 0)
                this.transform.Translate(dir, Space.World);
            this.transform.position = new Vector3(this.transform.position.x, originalY, this.transform.position.z);
        }
        else
        {
            this.transform.position += 5 * Time.deltaTime * Vector3.down;
        }

        /* Lock/unlock mouse on click */
        /*if (Input.GetMouseButtonUp(0))
        {
            if (!this.cursorVisible)
                this.UnlockMouse();
            else
                this.LockMouse();
        }*/
        if (Input.GetMouseButtonUp(0))
            if (this.BlockCast(this.transform.position, this.transform.forward, 3, out Vector3 position))
            {
            }
    }

    public bool BlockCast(Vector3 position, Vector3 direction, int length, out Vector3 actualPos)
    {
        World.Structure.World world = FindObjectOfType<World.Structure.World>();
        actualPos = new Vector3();
        for (int i = 0; i < length * 100; i++)
        {
            actualPos = position + direction.normalized * i / 100f;

            if (world.GetBlock(Mathf.FloorToInt(actualPos.x), Mathf.FloorToInt(actualPos.y),
                    Mathf.FloorToInt(actualPos.z)).Id == 0)
                continue;
            return true;
        }

        return false;
    }

    private void UnlockMouse()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        this.cursorVisible = true;
    }

    private void LockMouse()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        this.cursorVisible = false;
    }
}