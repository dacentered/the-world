﻿using System;
using System.Collections.Generic;
using UI.Screens;
using UnityEngine;

namespace UI
{
    public class UIScreenSwitcher : MonoBehaviour
    {
        public List<UIScreen> AllScreens;
        public UIScreen Current;
        public List<Transition> Transitions;

        public static UIScreenSwitcher Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            var transitionList1 = new List<Transition>();
            var transitionList2 = transitionList1;
            DefaultTransition defaultTransition1 = new DefaultTransition
            {
                NewScreen = typeof(GameScreen), PrevScreen = typeof(PauseScreen)
            };
            DefaultTransition defaultTransition2 = defaultTransition1;
            transitionList2.Add(defaultTransition2);
            var transitionList3 = transitionList1;
            DefaultTransition defaultTransition3 = new DefaultTransition
            {
                NewScreen = typeof(PauseScreen), PrevScreen = typeof(GameScreen)
            };
            DefaultTransition defaultTransition4 = defaultTransition3;
            transitionList3.Add(defaultTransition4);
            var transitionList4 = transitionList1;
            DefaultTransition defaultTransition5 = new DefaultTransition
            {
                NewScreen = typeof(GameScreen), PrevScreen = typeof(PregenerateScreen)
            };
            DefaultTransition defaultTransition6 = defaultTransition5;
            transitionList4.Add(defaultTransition6);
            this.Transitions = transitionList1;
            this.Current = this.AllScreens.Find(screen => screen.GetType() == typeof(PregenerateScreen));
        }

        public void ChangeScreen<T>() where T : UIScreen
        {
            UIScreen newScreen = this.AllScreens.Find(screen => screen.GetType() == typeof(T));
            Transition transition1 = this.Transitions.Find(transition =>
            {
                if (transition.PrevScreen == this.Current.GetType())
                    return transition.NewScreen == typeof(T);
                return false;
            });
            if (transition1 == null)
                return;
            transition1.Perform(this.Current, newScreen);
            this.Current = newScreen;
        }

        public abstract class Transition
        {
            public Type PrevScreen { get; set; }

            public Type NewScreen { get; set; }

            public abstract void Perform(UIScreen prevScreen, UIScreen newScreen);
        }

        public class DefaultTransition : Transition
        {
            public override void Perform(UIScreen prevScreen, UIScreen newScreen)
            {
                prevScreen.OnHide(newScreen);
                prevScreen.RootGameObject.SetActive(false);
                newScreen.RootGameObject.SetActive(true);
                newScreen.OnShow(prevScreen);
            }
        }
    }
}