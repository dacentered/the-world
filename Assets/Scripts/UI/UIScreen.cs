﻿using UnityEngine;

namespace UI
{
    public abstract class UIScreen : MonoBehaviour
    {
        public GameObject RootGameObject;

        public abstract void OnShow<T>(T previousScreen) where T : UIScreen;

        public abstract void OnHide<T>(T nextScreen) where T : UIScreen;

        public virtual void Update()
        {
        }
    }
}