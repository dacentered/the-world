﻿using UnityEngine;

namespace UI.Screens
{
    public class GameScreen : UIScreen
    {
        public override void OnShow<T>(T previousScreen) => this.GetComponent<AudioSource>().Play();

        public override void OnHide<T>(T nextScreen) => this.GetComponent<AudioSource>().Stop();

        public override void Update()
        {
            if (!Input.GetKeyUp(KeyCode.Escape))
                return;
            UIScreenSwitcher.Instance.ChangeScreen<PauseScreen>();
        }
    }
}