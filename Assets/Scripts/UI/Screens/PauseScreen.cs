﻿using UnityEngine;

namespace UI.Screens
{
    public class PauseScreen : UIScreen
    {
        public override void OnShow<T>(T previousScreen)
        {
            Time.timeScale = 0.0f;
            this.GetComponent<AudioSource>().Play();
        }

        public override void OnHide<T>(T nextScreen)
        {
            Time.timeScale = 1f;
            this.GetComponent<AudioSource>().Stop();
        }

        public override void Update()
        {
            if (!Input.GetKeyUp(KeyCode.Escape))
                return;
            UIScreenSwitcher.Instance.ChangeScreen<GameScreen>();
        }
    }
}