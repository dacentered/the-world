﻿using System.Collections;

using UnityEngine;

using World.Creation;
using SubsystemManager = Core.SubsystemManager;

namespace UI.Screens
{
    internal class PregenerateScreen : UIScreen
    {
        public int TotalChunks;


        public World.Structure.World world;

        private bool Generated => this.GeneratedChunksCount == this.TotalChunks && this.TotalChunks > 0;

        public int GeneratedChunksCount { get; set; }

        public override void OnShow<T>(T previousScreen) => this.GetComponent<AudioSource>().Play();

        public override void OnHide<T>(T nextScreen) => this.GetComponent<AudioSource>().Stop();

        public void OnPregenerate()
        {
            this.world = World.Structure.World.Create();
            ChunkProducerSubsystem producerSubsytem = SubsystemManager.Get<ChunkProducerSubsystem>();
            int area = SubsystemManager.Get<ChunkProducerSubsystem>().parameters.VisionSize;
            producerSubsytem.InitWorld(this.world, area);
            producerSubsytem.Pregenerate(this.world, area);
            SubsystemManager.StartAllSystems();

        }

        public void OnStart()
        {
            this.world.Pregenerated = true;
            UIScreenSwitcher.Instance.ChangeScreen<GameScreen>();
        }

        private IEnumerator UpdateProgress()
        {
            do
            {
                this.TotalChunks = this.world.TotalChunks;
                yield return new WaitForSeconds(0.3f);
            } while (!this.Generated);
        }
    }
}