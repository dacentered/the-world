using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Core.Memory
{
    public unsafe class MemorySubsystem : Subsystem
    {
        public static void* Allocate<T>(int count, Allocator allocator = Allocator.Persistent) where T : struct
        {
            var pointer =
                UnsafeUtility.Malloc(count * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
            return pointer;
        }

        public static void Release(void* pointer, Allocator allocator = Allocator.Persistent) =>
            UnsafeUtility.Free(pointer, allocator);
    }
}