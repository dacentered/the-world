﻿namespace Core.JobSystem
{
    public interface IJobExecutor
    {
        JobHandle AddJob<T>(T job, byte priority=127) where T : struct, IJob;

        void Run();

        void Stop();
    }
}