﻿namespace Core.JobSystem
{
    public interface IJob
    {
        void Execute();
    }

    public interface IJobResult<out T> : IJob
    {
        T GetResult();
    }
}