﻿using UnityEngine;

namespace Core.JobSystem
{
    [CreateAssetMenu]
    public class JobSystemSettings : SubsystemSettings<JobSystem>
    {
    }
}