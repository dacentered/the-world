﻿using System;

namespace Core.JobSystem
{
    public class JobHandle
    {
        public IJob ReferencedJob;

        public event Action<IJob> OnEnd;

        public void Complete()
        {
            var onEnd = this.OnEnd;
            onEnd?.Invoke(this.ReferencedJob);
        }
    }
}