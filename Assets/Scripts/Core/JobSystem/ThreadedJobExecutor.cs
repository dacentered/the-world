﻿using System.Collections.Generic;
using System.Threading;
using UnityEngine.Profiling;
using Utils.ConcurrentPriorityQueue;

namespace Core.JobSystem
{
    public class ThreadedJobExecutor : IJobExecutor
    {
        private readonly AutoResetEvent evnt;
        private readonly List<Thread> threads;
        public bool IsRunning;
        private readonly ConcurrentPriorityQueue<JobHandle, byte> jobs;

        public ThreadedJobExecutor(int threadCount)
        {
            this.jobs = new ConcurrentPriorityQueue<JobHandle, byte>();


            this.threads = new List<Thread>(threadCount);

            for (int index = 0; index < threadCount; ++index)
                this.threads.Add(new Thread(this.ExecutionThread));


            this.evnt = new AutoResetEvent(false);
        }

        public JobHandle AddJob<T>(T job, byte priority = 127) where T : struct, IJob
        {
            JobHandle jobHandle = new JobHandle
            {
                ReferencedJob = job
            };

            this.jobs.Enqueue(jobHandle, priority);
            this.evnt.Set();
            return jobHandle;
        }

        public void Run()
        {
            this.IsRunning = true;
            foreach (Thread thread in this.threads)
                thread.Start();
        }

        public void Stop() => this.IsRunning = false;

        private void ExecutionThread()
        {
            Profiler.BeginThreadProfiling("My Jobs", "Thread");
            while (this.IsRunning)
            {
                this.evnt.WaitOne();
                while (this.jobs.Count > 0)
                    if (this.jobs.TryDequeue(out JobHandle element))
                    {
                        element.ReferencedJob.Execute();
                        element.Complete();
                    }
            }
        }
    }
}