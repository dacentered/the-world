﻿using System;

namespace Core.JobSystem
{
    [AutoStart]
    public class JobSystem : UpdateableSubsystem
    {
        public ThreadedJobExecutor Threaded;
        public UnityJobExecutor Unity;


        public override void Initialize(SubsystemSettings settings)
        {
            base.Initialize(settings);
            this.Unity = new UnityJobExecutor();
            this.Threaded = new ThreadedJobExecutor(Environment.ProcessorCount);
        }

        public override void Run()
        {
            base.Run();
            this.Unity.Run();
            this.Threaded.Run();
        }

        public override void Tick()
        {
            if (this.IsRunning) this.Unity.Tick();
        }

        public override void Stop()
        {
            base.Stop();
            this.Unity.Stop();
            this.Threaded.Stop();
        }
    }
}