﻿using UnityEngine;
using UnityEngine.Profiling;
using Utils.ConcurrentPriorityQueue;

namespace Core.JobSystem
{
    public class UnityJobExecutor : IJobExecutor
    {
        private readonly double maxUpdateTime = 5.0;
        public bool IsRunning;
        private readonly ConcurrentPriorityQueue<JobHandle, byte> mainThreadJobs;

        public UnityJobExecutor() => this.mainThreadJobs = new ConcurrentPriorityQueue<JobHandle, byte>();

        public void Run() => this.IsRunning = true;

        public void Stop() => this.IsRunning = false;

        public JobHandle AddJob<T>(T job, byte priority = 127) where T : struct, IJob
        {
            JobHandle jobHandle = new JobHandle
            {
                ReferencedJob = job
            };
            this.mainThreadJobs.Enqueue(jobHandle, priority);
            return jobHandle;
        }

        public void Tick()
        {
            if (!this.IsRunning)
                return;
            double sinceStartupAsDouble = Time.realtimeSinceStartupAsDouble;
            while (this.mainThreadJobs.TryDequeue(out JobHandle result))
            {
                Profiler.BeginSample(result.ReferencedJob.GetType().Name);
                result.ReferencedJob.Execute();
                result.Complete();
                Profiler.EndSample();
                if (Time.realtimeSinceStartupAsDouble - sinceStartupAsDouble > this.maxUpdateTime / 1000.0)
                    break;
            }
        }
    }
}