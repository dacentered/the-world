﻿using System;

namespace Core
{
    public struct Mutable<T>
    {
        private T value;

        public T Value
        {
            get => this.value;
            set
            {
                T oldValue = this.value;
                this.value = value;
                this.OnChanged(oldValue, value);
            }
        }

        public event Action<T, T> Changed;

        public static implicit operator T(Mutable<T> mutable) => mutable.Value;

        public void Set(T newValue) => this.Value = newValue;

        private void OnChanged(T oldValue, T newValue) => this.Changed?.Invoke(oldValue, newValue);
    }
}