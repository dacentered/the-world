﻿namespace Core
{
    public abstract class UpdateableSubsystem : Subsystem
    {
        public abstract void Tick();
    }
}