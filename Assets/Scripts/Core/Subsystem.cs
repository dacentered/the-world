﻿namespace Core
{
    public abstract class Subsystem
    {
        public bool IsRunning { get; private set; }


        public virtual void Initialize(SubsystemSettings settings)
        {
        }

        public virtual void Stop() => this.IsRunning = false;

        public virtual void Run() => this.IsRunning = true;
    }
}