﻿using UnityEngine;

namespace Core
{
    public class SubsystemProxy : MonoBehaviour
    {
        public SubsystemManager Manager;

        private void Awake()
        {
            this.Manager = new SubsystemManager();
            this.Manager.Initialize(null);
        }

        private void Start() => this.Manager.Run();

        private void OnDestroy() => this.Manager.Stop();

        private void Update() => this.Manager.Tick();
    }
}