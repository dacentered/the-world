﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Profiling;

namespace Core
{
    [AutoStart]
    public class SubsystemManager : UpdateableSubsystem
    {
        private static SubsystemManager instance;
        public List<Subsystem> subsystems;
        public List<UpdateableSubsystem> UpdateableSubsystems;

        public static T Get<T>() where T : Subsystem => instance.subsystems.Find(s => s.GetType() == typeof(T)) as T;

        public override void Initialize(SubsystemSettings settings)
        {
            base.Initialize(settings);
            instance = this;
            this.subsystems = this.BuildSubsystems();

            this.UpdateableSubsystems = this.subsystems
                .OfType<UpdateableSubsystem>().ToList();

            this.InitializeSubsystems();
        }


        public override void Run() => this.RubSubsystems();

        public override void Stop() => this.StopSubsystems();

        public override void Tick() => this.UpdateSubsystems();


        private void RubSubsystems()
        {
            foreach (Subsystem subsystem in this.subsystems.Where(s =>
                s.GetType().GetCustomAttribute<AutoStartAttribute>() != null))
            {
                Debug.Log($"Subsystem {subsystem.GetType().Name} starting");
                subsystem.Run();
            }
        }

        private void StopSubsystems()
        {
            foreach (Subsystem subsystem in this.subsystems)
            {
                Debug.Log($"Subsystem {subsystem.GetType().Name} stopping");
                subsystem.Stop();
            }
        }

        private void UpdateSubsystems()
        {
            foreach (UpdateableSubsystem updateableSubsystem in this.UpdateableSubsystems.Where(s => s.IsRunning))
            {
                Profiler.BeginSample(updateableSubsystem.GetType().Name);
                updateableSubsystem.Tick();
                Profiler.EndSample();
            }
        }


        public static void StartAllSystems()
        {
            foreach (Subsystem subsystem in instance.subsystems.Where(s => !s.IsRunning))
            {
                Debug.Log($"Subsystem {subsystem.GetType().Name} starting");
                subsystem.Run();
            }
        }

        private void InitializeSubsystems()
        {
            Debug.Log($"Detected {(object) this.subsystems.Count} subsystems");
            foreach (Subsystem subsystem in this.subsystems)
            {
                Debug.Log($"Subsystem {subsystem.GetType().Name} initializing");
                subsystem.Initialize((SubsystemSettings) Resources.Load($"{subsystem.GetType().Name}.settings",
                    typeof(SubsystemSettings)));
            }
        }

        private List<Subsystem> BuildSubsystems() =>
            (from type in AppDomain.CurrentDomain.GetAssemblies().SelectMany(ass => ass.GetTypes())
                where type.IsSubclassOf(typeof(Subsystem)) && type != this.GetType() && !type.IsAbstract
                select Activator.CreateInstance(type) as Subsystem).ToList();
    }

    public class AutoStartAttribute : Attribute
    {
    }
}