﻿using System;
using System.Runtime.InteropServices;
using Core.Memory;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Scripting;

[assembly: Preserve]
[assembly: AlwaysLinkAssembly]

namespace Utils
{
    public unsafe struct FlattenedArray<T> : IDisposable where T : struct
    {
        private readonly void* array;

        public readonly int XSize;
        public readonly int YSize;
        public readonly int ZSize;

        public bool Created;

        private readonly Allocator allocator;

        public FlattenedArray(int xSize, int ySize, int zSize, Allocator allocator = Allocator.Persistent)
        {
            this.XSize = xSize;
            this.YSize = ySize;
            this.ZSize = zSize;
            this.array = MemorySubsystem.Allocate<T>(xSize * ySize * zSize, allocator);
            this.allocator = allocator;
            this.Created = true;
        }

        public T this[int x, int y, int z]
        {
            get => UnsafeUtility.ReadArrayElement<T>(this.array, z * this.XSize * this.YSize + y * this.ZSize + x);
            set => UnsafeUtility.WriteArrayElement(this.array, z * this.XSize * this.YSize + y * this.ZSize + x, value);
        }

        public T[] ToArray()
        {
            var a = new T[this.XSize * this.YSize * this.ZSize];
            GCHandle gcHandle = GCHandle.Alloc(a, GCHandleType.Pinned);
            UnsafeUtility.MemCpy((void*) (IntPtr) (void*) gcHandle.AddrOfPinnedObject(), (void*) (IntPtr) this.array,
                this.XSize * this.YSize * this.ZSize * UnsafeUtility.SizeOf<T>());
            gcHandle.Free();
            return a;
        }

        public void Dispose()
        {
            this.Created = false;
            MemorySubsystem.Release(this.array, this.allocator);
        }

        public void Clear() =>
            UnsafeUtility.MemSet(this.array, 0, this.XSize * this.YSize * this.ZSize * UnsafeUtility.SizeOf<T>());
    }
}