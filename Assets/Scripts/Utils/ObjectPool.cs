﻿using System.Collections.Concurrent;
using UnityEngine;
using UnityEngine.Profiling;

namespace Utils
{
    public abstract class ObjectPool<T>
    {
        protected readonly ConcurrentStack<T> Stack;

        protected ObjectPool() => this.Stack = new ConcurrentStack<T>();

        protected abstract T Create();

        protected abstract void Clear(T obj);

        public T Instantiate() => this.Stack.TryPop(out T a) ? a : this.Create();

        public void Return(T obj)
        {
            if (obj == null) return;
            this.Clear(obj);
            Profiler.BeginSample("Pushing to stack");
            this.Stack.Push(obj);
            Profiler.EndSample();
        }

        public void Pregenerate(int count)
        {
            for (int i = 0; i < count; i++)
            {
                T obj = this.Create();
                this.Clear(obj);
                this.Stack.Push(obj);
            }
        }
    }

    public class MeshPool : ObjectPool<Mesh>
    {
        protected override Mesh Create() => new Mesh();

        protected override void Clear(Mesh obj)
        {
            Profiler.BeginSample("Mesh cleaning");
            obj.Clear();
            Profiler.EndSample();
        }
    }


    public class ColliderPool : ObjectPool<BoxCollider>
    {
        protected GameObject Prefab;
        public ColliderPool(GameObject prefab) => this.Prefab = prefab;

        protected override BoxCollider Create() => Object.Instantiate(this.Prefab).GetComponent<BoxCollider>();

        protected override void Clear(BoxCollider obj)
        {
            Profiler.BeginSample("Deactivating gameobject");
            //obj.gameObject.hideFlags = HideFlags.HideInHierarchy;
            obj.gameObject.SetActive(false);
            Profiler.EndSample();
        }
    }
}