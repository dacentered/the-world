﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Utils.ConcurrentPriorityQueue
{
    /// <summary>
    ///     Heap-based implementation of priority queue.
    /// </summary>
    public abstract class AbstractPriorityQueue<TElement, TPriority> : IPriorityQueue<TElement, TPriority>
        where TPriority : IComparable<TPriority>
    {
        internal readonly NodeComparer _comparer;
        private readonly bool _dataIsValueType;
        internal int _count;

        internal Node[] _nodes;

        /// <summary>
        ///     Create an empty max priority queue of given capacity.
        /// </summary>
        /// <param name="capacity">Queue capacity. Greater than 0.</param>
        /// <param name="comparer">Priority comparer. Default type comparer will be used unless custom is provided.</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        internal AbstractPriorityQueue(int capacity, IComparer<TPriority> comparer = null)
        {
            if (capacity <= 0)
                throw new ArgumentOutOfRangeException("capacity", "Expected capacity greater than zero.");

            this._nodes = new Node[capacity + 1]; // first element at 1
            this._count = 0;
            this._comparer = new NodeComparer(comparer ?? Comparer<TPriority>.Default);
            this._dataIsValueType = typeof(TElement).IsValueType;
        }

        /// <summary>
        ///     Create a new priority queue from the given nodes storage and comparer.
        ///     Used to create existing queue copies.
        /// </summary>
        /// <param name="nodes">Heap with data.</param>
        /// <param name="count">Count of items in the heap.</param>
        /// <param name="comparer">Node comparer for nodes in the queue.</param>
        internal AbstractPriorityQueue(Node[] nodes, int count, NodeComparer comparer)
        {
            this._nodes = nodes;
            this._count = count;
            this._comparer = comparer;
            this._dataIsValueType = typeof(TElement).IsValueType;
        }

        public int Capacity => this._nodes.Length - 1;

        public int Count => this._count;

        /// <summary>
        ///     Returns true if there is at least one item, which is equal to given.
        ///     TElement.Equals is used to compare equality.
        /// </summary>
        public virtual bool Contains(TElement item) => this.GetItemIndex(item) > 0;

        public virtual void Enqueue(TElement item, TPriority priority)
        {
            int index = this._count + 1;
            this._nodes[index] = new Node(item, priority);

            this._count = index; // update count after the element is really added but before Sift

            this.Sift(index); // move item "up" while heap principles are not met
        }

        public virtual TElement Dequeue()
        {
            if (this._count == 0) throw new InvalidOperationException("Unable to dequeue from empty queue.");

            TElement item = this._nodes[1].Element; // first element at 1
            this.Swap(1, this._count); // last element at _count
            this._nodes[this._count] = null; // release hold on the object

            this._count--; // update count after the element is really gone but before Sink

            this.Sink(1); // move item "down" while heap principles are not met

            return item;
        }

        /// <summary>
        ///     Returns the first element in the queue (element with max priority) without removing it from the queue.
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        public virtual TElement Peek()
        {
            if (this._count == 0) throw new InvalidOperationException("Unable to peek from empty queue.");

            return this._nodes[1].Element; // first element at 1
        }

        /// <summary>
        ///     Remove all items from the queue. Capacity is not changed.
        /// </summary>
        public virtual void Clear()
        {
            for (int i = 1; i <= this._count; i++) this._nodes[i] = null;
            this._count = 0;
        }

        /// <summary>
        ///     Update priority of the first occurrence of the given item
        /// </summary>
        /// <param name="item">Item, which priority should be updated.</param>
        /// <param name="priority">New priority</param>
        /// <exception cref="ArgumentException"></exception>
        public virtual void UpdatePriority(TElement item, TPriority priority)
        {
            int index = this.GetItemIndex(item);
            if (index == 0) throw new ArgumentException("Item is not found in the queue.");

            int priorityCompare = this._comparer.Compare(this._nodes[index].Priority, priority);
            if (priorityCompare < 0)
            {
                this._nodes[index].Priority = priority;
                this.Sift(index); // priority is increased, so item should go "up" the heap
            }
            else if (priorityCompare > 0)
            {
                this._nodes[index].Priority = priority;
                this.Sink(index); // priority is decreased, so item should go "down" the heap
            }
        }

        public abstract IEnumerator<TElement> GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        /// <summary>
        ///     Returns index of the first occurrence of the given item or 0.
        ///     TElement.Equals is used to compare equality.
        /// </summary>
        private int GetItemIndex(TElement item)
        {
            for (int i = 1; i <= this._count; i++)
                if (this.Equals(this._nodes[i].Element, item))
                    return i;
            return 0;
        }

        /// <summary>
        ///     Check if given data items are equal using TD.Equals.
        ///     Handles null values for object types.
        /// </summary>
        internal bool Equals(TElement a, TElement b)
        {
            if (this._dataIsValueType) return a.Equals(b);

            object objA = a;
            object objB = b;
            if (objA == null && objB == null) return true; // null == null because equality should be symmetric
            if (objA == null || objB == null) return false;
            return objA.Equals(objB);
        }

        public virtual bool TryDequeue(out TElement element)
        {
            element = default;
            if (this._count == 0) return false;

            TElement item = this._nodes[1].Element; // first element at 1
            this.Swap(1, this._count); // last element at _count
            this._nodes[this._count] = null; // release hold on the object

            this._count--; // update count after the element is really gone but before Sink

            this.Sink(1); // move item "down" while heap principles are not met
            element = item;
            return true;
        }

        /// <summary>
        ///     Returns a copy of internal heap array. Number of elements is _count + 1;
        /// </summary>
        /// <returns></returns>
        internal Node[] CopyNodes()
        {
            var nodesCopy = new Node[this._count + 1];
            Array.Copy(this._nodes, 0, nodesCopy, 0, this._count + 1);
            return nodesCopy;
        }

        private bool GreaterOrEqual(Node i, Node j) => this._comparer.Compare(i, j) >= 0;

        /// <summary>
        ///     Moves the item with given index "down" the heap while heap principles are not met.
        /// </summary>
        private void Sink(int i)
        {
            while (true)
            {
                int leftChildIndex = 2 * i;
                int rightChildIndex = 2 * i + 1;
                if (leftChildIndex > this._count) return; // reached last item

                Node item = this._nodes[i];
                Node left = this._nodes[leftChildIndex];
                Node right = rightChildIndex > this._count ? null : this._nodes[rightChildIndex];

                // if item is greater than children - exit
                if (this.GreaterOrEqual(item, left) && (right == null || this.GreaterOrEqual(item, right))) return;

                // else exchange with greater of children
                int greaterChild = right == null || this.GreaterOrEqual(left, right) ? leftChildIndex : rightChildIndex;
                this.Swap(i, greaterChild);

                // continue at new position
                i = greaterChild;
            }
        }

        /// <summary>
        ///     Moves the item with given index "up" the heap while heap principles are not met.
        /// </summary>
        private void Sift(int i)
        {
            while (true)
            {
                if (i <= 1) return; // reached root
                int parent = i / 2; // get parent

                // if root is greater or equal - exit
                if (this.GreaterOrEqual(this._nodes[parent], this._nodes[i])) return;

                this.Swap(parent, i);
                i = parent;
            }
        }

        private void Swap(int i, int j)
        {
            Node tmp = this._nodes[i];
            this._nodes[i] = this._nodes[j];
            this._nodes[j] = tmp;
        }

        internal sealed class Node
        {
            public readonly TElement Element;

            public Node(TElement element, TPriority priority)
            {
                this.Priority = priority;
                this.Element = element;
            }

            public TPriority Priority { get; internal set; }
        }

        /// <summary>
        ///     Compare nodes based on priority or just priorities
        /// </summary>
        internal sealed class NodeComparer : IComparer<Node>, IComparer<TPriority>
        {
            private readonly IComparer<TPriority> _comparer;

            public NodeComparer(IComparer<TPriority> comparer) => this._comparer = comparer;

            public int Compare(Node x, Node y)
            {
                if (x == null && y == null) return 0;
                if (x == null) return -1;
                if (y == null) return 1;

                return this._comparer.Compare(x.Priority, y.Priority);
            }

            public int Compare(TPriority x, TPriority y) => this._comparer.Compare(x, y);
        }

        /// <summary>
        ///     Queue items enumerator. Returns items in the oder of queue priority.
        /// </summary>
        internal sealed class PriorityQueueEnumerator : IEnumerator<TElement>
        {
            private readonly TElement[] _items;
            private int _currentIndex;

            internal PriorityQueueEnumerator(AbstractPriorityQueue<TElement, TPriority> queueCopy)
            {
                this._items = new TElement[queueCopy.Count];

                // dequeue the given queue copy to extract items in order of priority
                // enumerator is based on the new array to allow reset and multiple enumerations
                for (int i = 0; i < this._items.Length; i++) this._items[i] = queueCopy.Dequeue();

                this.Reset();
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                this._currentIndex++;
                return this._currentIndex < this._items.Length;
            }

            public void Reset() => this._currentIndex = -1;

            public TElement Current => this._items[this._currentIndex];

            object IEnumerator.Current => this.Current;
        }
    }
}