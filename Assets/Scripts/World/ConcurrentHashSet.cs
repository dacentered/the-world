﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace World
{
    public class ConcurrentHashSet<T> : IDisposable
    {
        private readonly HashSet<T> _hashSet = new HashSet<T>();
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

        #region Dispose

        public void Dispose()
        {
            if (this._lock != null) this._lock.Dispose();
        }

        #endregion

        #region Implementation of ICollection<T> ...ish

        public bool Add(T item)
        {
            try
            {
                this._lock.EnterWriteLock();
                return this._hashSet.Add(item);
            }
            finally
            {
                if (this._lock.IsWriteLockHeld) this._lock.ExitWriteLock();
            }
        }

        public void Clear()
        {
            try
            {
                this._lock.EnterWriteLock();
                this._hashSet.Clear();
            }
            finally
            {
                if (this._lock.IsWriteLockHeld) this._lock.ExitWriteLock();
            }
        }

        public bool Contains(T item)
        {
            try
            {
                this._lock.EnterReadLock();
                return this._hashSet.Contains(item);
            }
            finally
            {
                if (this._lock.IsReadLockHeld) this._lock.ExitReadLock();
            }
        }

        public bool Remove(T item)
        {
            try
            {
                this._lock.EnterWriteLock();
                return this._hashSet.Remove(item);
            }
            finally
            {
                if (this._lock.IsWriteLockHeld) this._lock.ExitWriteLock();
            }
        }

        public int Count
        {
            get
            {
                try
                {
                    this._lock.EnterReadLock();
                    return this._hashSet.Count;
                }
                finally
                {
                    if (this._lock.IsReadLockHeld) this._lock.ExitReadLock();
                }
            }
        }

        #endregion
    }
}