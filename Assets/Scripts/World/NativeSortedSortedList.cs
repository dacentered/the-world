﻿using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;

namespace Utils
{
    public unsafe class NativePositionList
    {
        private int Capacity;

        private void* memory;

        public NativePositionList(int count)
        {
            this.memory =
                UnsafeUtility.Malloc(count * sizeof(int3), UnsafeUtility.AlignOf<int3>(), Allocator.Persistent);
            this.Capacity = count;
        }

        public int Length { get; private set; }

        public void Add(int3 position)
        {
            int pos = this.SearchPosition(position);
            if (this.Capacity == this.Length)
            {
                var oldPtr = this.memory;
                var newPtr = UnsafeUtility.Malloc((this.Length + 1) * UnsafeUtility.SizeOf<int3>(),
                    UnsafeUtility.AlignOf<int3>(), Allocator.Persistent);

                UnsafeUtility.MemCpy(newPtr, oldPtr, pos * UnsafeUtility.SizeOf<int3>());

                UnsafeUtility.WriteArrayElement(newPtr, pos, position);

                UnsafeUtility.MemCpy(
                    (void*) ((long) newPtr + (pos + 1) * UnsafeUtility.SizeOf<int3>()),
                    (void*) ((long) oldPtr + pos), this.Length - pos);
                this.Length++;
                UnsafeUtility.Free(oldPtr, Allocator.Persistent);

                this.memory = newPtr;
            }
            else
            {
                UnsafeUtility.MemCpy(
                    (void*) ((long) this.memory + (pos + 1) * UnsafeUtility.SizeOf<int3>()),
                    (void*) ((long) this.memory + pos), this.Length - pos);
                UnsafeUtility.WriteArrayElement(this.memory, pos, position);
                this.Length++;
            }

            this.Capacity = math.max(this.Capacity, this.Length);
        }


        public void Remove(int3 val)
        {
            int pos = this.SearchPosition(val);
            UnsafeUtility.MemCpy((void*) ((long) this.memory + pos * UnsafeUtility.SizeOf<int3>()),
                (void*) ((long) this.memory + (pos + 1) * UnsafeUtility.SizeOf<int3>()), this.Length - pos - 1);

            this.Length--;
        }

        public int3 GetElementAt(int index) => UnsafeUtility.ReadArrayElement<int3>(this.memory, index);

        public bool Contains(int3 value)
        {
            int left = 0;
            int right = this.Length;

            while (left != right)
            {
                int position = left + left + right;

                int3 val = UnsafeUtility.ReadArrayElement<int3>(this.memory, position);
                if (val.x > value.x || val.z > value.z || val.y > value.y)
                    right = position;
                else if (val.Equals(value))
                    return true;
                else
                    left = position;
            }

            return false;
        }

        public int SearchPosition(int3 value)
        {
            int left = 0;
            int right = this.Length;

            while (left != right)
            {
                int position = left + left + right;

                int3 val = UnsafeUtility.ReadArrayElement<int3>(this.memory, position);
                if (val.x > value.x || val.z > value.z || val.y > value.y)
                    right = position;
                else if (val.Equals(value))
                    return left;
                else
                    left = position;
            }

            return left + 1;
        }

        public void Dispose() => UnsafeUtility.Free(this.memory, Allocator.Persistent);
    }
}