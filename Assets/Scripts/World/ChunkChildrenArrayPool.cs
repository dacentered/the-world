﻿using System.Collections.Concurrent;
using Core.Memory;
using Unity.Collections.LowLevel.Unsafe;

namespace Utils
{
    public static unsafe class ChunkChildrenArrayPool
    {
        private static readonly ConcurrentStack<long> Free = new ConcurrentStack<long>();

        private static readonly ConcurrentBag<(long p, long freeSize, long maxSize)> AllAllocations =
            new ConcurrentBag<(long p, long freeSize, long maxSize)>();


        public static void PregenerateMassive(int count)
        {
            var pointer = MemorySubsystem.Allocate<OctreeChildArray>(count);

            AllAllocations.Add(((long) pointer, count, count));
            for (int i = 0; i < count; i++)
            {
                Free.Push((long) pointer + i * UnsafeUtility.SizeOf<OctreeChildArray>());
                UnsafeUtility.WriteArrayElement(pointer, i, new OctreeChildArray(true));
            }
        }

        public static long Get()
        {
            if (Free.TryPop(out long allocation)) return allocation;

            var pointer = MemorySubsystem.Allocate<OctreeChildArray>(1);
            OctreeChildArray octreeChildArray = new OctreeChildArray(true);
            UnsafeUtility.CopyStructureToPtr(ref octreeChildArray, pointer);
            AllAllocations.Add(((long) pointer, 1, 1));
            return (long) pointer;
        }

        public static void Return(long pointer) => Free.Push(pointer);


        public static void Dispose()
        {
            foreach ((long l, long _, long maxSize) in AllAllocations)
                for (int i = 0; i < maxSize; i++)
                    //((OctreeChildArray*) l+i*UnsafeUtility.SizeOf<OctreeChildArray>())->Release();
                    MemorySubsystem.Release((void*) l);
        }
    }


    public static unsafe class ChunkChildrenPool
    {
        private static readonly ConcurrentStack<long> Free = new ConcurrentStack<long>();

        private static readonly ConcurrentBag<(long p, long freeSize, long maxSize)> AllAllocations =
            new ConcurrentBag<(long p, long freeSize, long maxSize)>();


        public static void PregenerateMassive(int count)
        {
            var pointer = MemorySubsystem.Allocate<OctreeCollider>(8 * count);

            AllAllocations.Add(((long) pointer, count, count));
            for (int i = 0; i < count; i++) Free.Push((long) pointer + 8 * i * UnsafeUtility.SizeOf<OctreeCollider>());
        }

        public static long Get()
        {
            if (Free.TryPop(out long allocation))
            {
                return allocation;
            }

            var pointer = MemorySubsystem.Allocate<OctreeCollider>(8);
            AllAllocations.Add(((long) pointer, 1, 1));
            return (long) pointer;
        }

        public static void Return(long pointer) => Free.Push(pointer);


        public static void Dispose()
        {
            foreach ((long l, long _, long maxSize) in AllAllocations)
                for (int i = 0; i < maxSize; i++)
                    //((OctreeChildArray*) l+i*UnsafeUtility.SizeOf<OctreeChildArray>())->Release();
                    MemorySubsystem.Release((void*) l);
        }
    }
}