﻿using System.Collections.Generic;
using UnityEngine;

namespace World.Structure
{
    [CreateAssetMenu]
    public class BlockInfo : ScriptableObject
    {
        public static Dictionary<byte, BlockInfo> AllInfos;

        public bool GenerateTexture;
        public byte Id;

        public Texture2D Texture;

        public bool Transparent;

        public Vector2[] UVs;


        public static Texture2D BuildTexture()
        {
            AllInfos = new Dictionary<byte, BlockInfo>();
            var blocks = Resources.LoadAll<BlockInfo>("Blocks");

            Texture2D texture = new Texture2D(8 * 512, Mathf.CeilToInt(blocks.Length / 8f) * 512)
            {
                filterMode = FilterMode.Trilinear, anisoLevel = 16
            };

            foreach (BlockInfo blockInfo in blocks)
            {
                AllInfos.Add(blockInfo.Id, blockInfo);

                if (blockInfo.GenerateTexture)
                {
                    Texture2D blockTexture = blockInfo.Texture;

                    blockInfo.UVs = new[]
                    {
                        new Vector2(blockInfo.Id % 8 * 512f / texture.width,
                            Mathf.FloorToInt(blockInfo.Id / 8f) * 512f / texture.height),
                        new Vector2(blockInfo.Id % 8 * 512f / texture.width,
                            (Mathf.FloorToInt(blockInfo.Id / 8f) * 512f + 512f) / texture.height),
                        new Vector2((blockInfo.Id % 8 * 512f + 512) / texture.width,
                            (Mathf.FloorToInt(blockInfo.Id / 8f) * 512f + 512f) / texture.height),
                        new Vector2((blockInfo.Id % 8 * 512f + 512) / texture.width,
                            Mathf.FloorToInt(blockInfo.Id / 8f) * 512f / texture.height)
                    };
                    for (int i = 0, coef = 1; i < blockTexture.mipmapCount; i++, coef *= 2)
                        Graphics.CopyTexture(blockTexture, 0, i, 0, 0, blockTexture.width / coef,
                            blockTexture.height / coef, texture, 0, i,
                            blockInfo.Id % 8 * 512 / coef, Mathf.FloorToInt(blockInfo.Id / 8f) * 512 / coef);
                }
            }


            return texture;
        }


        public static BlockInfo GetBlockInfoForBlock(byte id) => AllInfos[id];
    }
}