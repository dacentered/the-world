﻿using Unity.Mathematics;
using UnityEngine;
using Utils;

namespace World.Structure
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class Chunk : MonoBehaviour
    {
        public FlattenedArray<Block> blocks;

        public int3 Position;
        public int Size;
        public World world;
        public MeshFilter MeshFilter { get; private set; }

        public MeshRenderer MeshRenderer { get; private set; }

        public Mesh Mesh { get; set; }


        private void Awake()
        {
            this.MeshFilter = this.GetComponent<MeshFilter>();
            this.MeshRenderer = this.GetComponent<MeshRenderer>();
        }
    }
}