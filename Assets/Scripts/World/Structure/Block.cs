﻿using System;
using UnityEngine;

namespace World.Structure
{
    [Serializable]
    public struct Block
    {
        private static Vector2[] StoneUV = new Vector2[4]
        {
            new Vector2(0.0f, 0.75f),
            new Vector2(0.0f, 1f),
            new Vector2(0.25f, 1f),
            new Vector2(0.25f, 0.75f)
        };

        private static Vector2[] GrassUV = new Vector2[4]
        {
            new Vector2(0.0f, 0.5f),
            new Vector2(0.0f, 0.75f),
            new Vector2(0.25f, 0.75f),
            new Vector2(0.25f, 0.5f)
        };

        public byte Id;
        public bool Transparent;

        public static Vector2[] GetUvsForBlock(Block block)
        {
            switch (block.Id)
            {
                case 1:
                    return GrassUV;
                case 2:
                    return StoneUV;
                default:
                    return null;
            }
        }
    }
}