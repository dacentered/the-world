﻿// Decompiled with JetBrains decompiler
// Type: World.World
// Assembly: World, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7305CABD-97CE-489F-869B-FDA1E33DABCA
// Assembly location: C:\Users\Vovan\Desktop\The World\Build\New Unity Project_Data\Managed\World.dll

using System.Collections.Concurrent;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace World.Structure
{
    public class World : MonoBehaviour
    {
        public NativeHashSet<int3> ChunkPositions;
        public ConcurrentDictionary<int3, Chunk> Chunks;
        public int ChunkSize;
        public bool Pregenerated;

        public int TotalChunks => this.ChunkPositions.Count();

        public static World Create() => new GameObject {name = nameof(World)}.AddComponent<World>();


        public Block GetBlock(int x, int y, int z)
        {
            int3 pos = this.ToChunkPos(x, y, z);
            int3 shift = new int3(x, y, z) - pos * this.ChunkSize;

            if (this.Chunks.TryGetValue(pos, out Chunk ch))
                return ch.blocks[shift.x, shift.y, shift.z];

            return new Block();
        }

        public Block GetBlock(float x, float y, float z)
        {
            int3 pos = this.ToChunkPos(x, y, z);
            int3 shift = (int3) new float3(x, y, z) - pos * this.ChunkSize;

            if (this.Chunks.TryGetValue(pos, out Chunk ch))
                return ch.blocks[shift.x, shift.y, shift.z];

            return new Block();
        }

        public int3 ToChunkPos(float x, float y, float z)
        {
            int3 pos = (int3) (new float3(x, y, z) / this.ChunkSize);
            if (x < 0 && x % 16 != 0) pos.x--;
            if (y < 0 && y % 16 != 0) pos.y--;
            if (z < 0 && z % 16 != 0) pos.z--;
            return pos;
        }

        public int3 ToChunkPos(int x, int y, int z)
        {
            int3 pos = (int3) (new float3(x, y, z) / this.ChunkSize);
            if (x < 0 && x % 16 != 0) pos.x--;
            if (y < 0 && y % 16 != 0) pos.y--;
            if (z < 0 && z % 16 != 0) pos.z--;
            return pos;
        }

        public void SetBlock(int x, int y, int z, Block block)
        {
            int3 pos = this.ToChunkPos(x, y, z);
            int3 shift = new int3(x, y, z) - pos * this.ChunkSize;


            if (this.Chunks.TryGetValue(pos, out Chunk ch))
            {
                Debug.Log("block actually setted");
                ch.blocks[shift.x, shift.y, shift.z] = block;
            }
        }
    }
}