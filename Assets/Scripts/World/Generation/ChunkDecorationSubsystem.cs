﻿using Unity.Mathematics;
using World.ChunkPipeline;
using World.Structure;

namespace World.Generation
{
    public class ChunkDecorator : ChunkProcessor
    {
        public ChunkDecorator() => this.Multithreaded = true;

        public override void Process(Chunk chunk)
        {
            Random random = new Random(355U);
            float3 shift = random.NextFloat3() * 10000f;
            //this.GenerateTrees(chunk,shift);
        }

        private void GenerateTrees(Chunk chunk, float3 shift)
        {
            for (int x = 0; x < chunk.blocks.XSize; ++x)
            for (int y = 0; y < chunk.blocks.YSize; ++y)
            for (int z = 0; z < chunk.blocks.ZSize; ++z)
                if (ShouldPlaceTree(chunk, y, x, z))
                {
                    float3 pos = chunk.Position * chunk.Size + shift + new float3(x, y, z);
                    this.GenerateTree(chunk, y, pos, x, z);
                }
        }

        private static bool ShouldPlaceTree(Chunk chunk, int y, int x, int z) => y > 1 && chunk.world.GetBlock(x, y - 1, z).Id == 3;

        private void GenerateTree(Chunk chunk, int startY, float3 pos, int x, int z)
        {
            for (int i = 0; i < 64; i++)
            {
                float yScale = math.clamp(48f - chunk.Position.y * chunk.Size - startY - i, 0f, 64f) / 64f;
                if (noise.cellular(pos.xz / 64f).x < 0.05f * yScale)
                    chunk.world.SetBlock(x + chunk.Position.x * chunk.Size,
                        startY + i + chunk.Position.y * chunk.Size,
                        z + chunk.Position.z * chunk.Size,
                        new Block {Id = 6});
            }
        }
    }
}