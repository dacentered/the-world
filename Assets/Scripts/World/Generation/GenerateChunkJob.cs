﻿using Core;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine.Profiling;
using Utils;
using World.ChunkPipeline;
using World.Structure;

namespace World.Generation
{
    [BurstCompile(CompileSynchronously = true)]
    public class ChunkGenerator : ChunkProcessor
    {
        private readonly GenerationSettings settings;


        public ChunkGenerator()
        {
            this.settings = SubsystemManager.Get<ChunkGeneratorSubsystem>().chunkGenerationSettings.settings;
            this.Multithreaded = true;
        }

        public override void Process(Chunk chunk)
        {
            if (chunk == null ||
                SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().GetState(chunk) is DestoyingChunkState)
                return;
            if (!chunk.blocks.Created)
                chunk.blocks = new FlattenedArray<Block>(chunk.Size, chunk.Size, chunk.Size);

            Random random = new Random(355U);
            float3 shift = random.NextFloat3() * 10000f;
            int3 chunkPosition = chunk.Position * chunk.Size;
            var noiseMap = this.GenerateNoiseMap(chunk, chunkPosition, shift);
            Profiler.BeginSample("Block generating");

            this.GenerateBaseBlocks(chunk, noiseMap, chunkPosition, shift);
            Profiler.EndSample();
            noiseMap.Dispose();
            this.GenerateQuartz(chunk, shift);
            this.GenerateCaves(chunk, shift);
        }

        private void GenerateBaseBlocks(Chunk chunk, FlattenedArray<float> noiseMap, int3 chunkPosition, float3 shift)
        {
            for (int x = 0; x < chunk.blocks.XSize; ++x)
                for (int y = 0; y < chunk.blocks.YSize; ++y)
                    for (int z = 0; z < chunk.blocks.ZSize; ++z)
                        chunk.blocks[x, y, z] = this.GetTheoreticalBlock(in noiseMap, x, y, z, chunkPosition, shift);
        }

        private FlattenedArray<float> GenerateNoiseMap(Chunk chunk, int3 chunkPosition, float3 shift)
        {
            var noiseMap =
                new FlattenedArray<float>(chunk.Size, chunk.Size, chunk.Size, Allocator.TempJob);

            for (int x = 0; x < chunk.blocks.XSize; ++x)
                for (int y = 0; y < chunk.blocks.YSize; ++y)
                    for (int z = 0; z < chunk.blocks.ZSize; ++z)
                        noiseMap[x, y, z] = this.CalculateNoise(x, y, z, chunkPosition, shift);
            return noiseMap;
        }


        [BurstCompile(CompileSynchronously = true)]
        private float CalculateNoise(int x, int y, int z, int3 chunkPosition, float3 shift)
        {
            float3 pos = new float3(x, y, z) + chunkPosition + shift;
            float noise = 0.0f;
            for (int i = 0; i < this.settings.MainNoiseSettings.Count; ++i)
            {
                NoiseOctaveSettings mainNoiseSetting = this.settings.MainNoiseSettings[i];
                noise += Noise.Generate(pos.x / mainNoiseSetting.Scale, pos.y / mainNoiseSetting.Scale,
                             pos.z / mainNoiseSetting.Scale) * mainNoiseSetting.NoiseScale;
            }

            float heightmapNoise = 0.0f;
            for (int i = 0; i < this.settings.HeightmapNoiseSettings.Count; ++i)
            {
                NoiseOctaveSettings heightmapNoiseSetting = this.settings.HeightmapNoiseSettings[i];
                heightmapNoise += Noise.Generate(pos.x / heightmapNoiseSetting.Scale,
                                      pos.z / heightmapNoiseSetting.Scale) * heightmapNoiseSetting.NoiseScale;
            }

            float noiseValue = noise + (heightmapNoise - (y + chunkPosition.y));
            return noiseValue * 10f;
        }

        [BurstCompile(CompileSynchronously = true)]
        private void GenerateQuartz(Chunk chunk, float3 shift)
        {
            for (int x = 0; x < chunk.blocks.XSize; ++x)
                for (int y = 0; y < chunk.blocks.YSize; ++y)
                    for (int z = 0; z < chunk.blocks.ZSize; ++z)
                        this.GenerateQuartzBlock(chunk, shift, x, y, z);
        }

        private void GenerateQuartzBlock(Chunk chunk, float3 shift, int x, int y, int z)
        {
            float3 pos = chunk.Position * chunk.Size + shift + new float3(x, y, z);
            if (Noise.Generate(pos.x / 32f, pos.y / 32f, pos.z / 32f) > 0.6f)
                if (chunk.blocks[x, y, z].Id == 2)
                    chunk.blocks[x, y, z] = new Block {Id = 5};
        }

        [BurstCompile(CompileSynchronously = true)]
        private void GenerateCaves(Chunk chunk, float3 shift)
        {
            for (int x = 0; x < chunk.blocks.XSize; ++x)
                for (int y = 0; y < chunk.blocks.YSize; ++y)
                    for (int z = 0; z < chunk.blocks.ZSize; ++z)
                        this.GenerateCaveBlock(chunk, shift, x, y, z);
        }

        private void GenerateCaveBlock(Chunk chunk, float3 shift, int x, int y, int z)
        {
            float3 pos = chunk.Position * chunk.Size + shift + new float3(x, y, z);
            float yScale = math.clamp((10 - chunk.Position.y * chunk.Size - y) / 64f * 0.13f + 0.88f,
                0.8f, 1.1f);
            float noiseValue = yScale * (Noise.Generate(pos.x / 64, pos.y / 32, pos.z / 64) / 4 + 1) *
                               noise.cellular(new float3(pos.x / 64, pos.y / 64, pos.z / 64)).x;
            if (noiseValue > 0.78f)
                chunk.blocks[x, y, z] = new Block {Id = 0};
        }


        private Block GetTheoreticalBlock(in FlattenedArray<float> noiseMap, int x, int y, int z, int3 chunkPosition,
            float3 shift)
        {
            float noiseValue = noiseMap[x, y, z];
            if (noiseValue > 64) return new Block {Id = 2};

            if (noiseValue > 2)
            {
                if (y < 16 && this.CalculateNoise(x, y + 1, z, chunkPosition, shift) < 2)
                    return new Block {Id = 3};
                return new Block {Id = 1};
            }

            return new Block {Id = 0};
        }
    }
}