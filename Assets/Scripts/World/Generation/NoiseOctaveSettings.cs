﻿using System;

namespace World.Generation
{
    [Serializable]
    public class NoiseOctaveSettings
    {
        public float NoiseScale;
        public float Scale;
    }
}