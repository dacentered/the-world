﻿using Core;

namespace World.Generation
{
    public class ChunkGeneratorSubsystem : Subsystem
    {
        public ChunkGeneratorSettings chunkGenerationSettings;

        public override void Initialize(SubsystemSettings set)
        {
            base.Initialize(set);
            this.chunkGenerationSettings = set as ChunkGeneratorSettings;
        }
    }
}