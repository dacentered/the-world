﻿using Core;
using UnityEngine;

namespace World.Generation
{
    [CreateAssetMenu]
    public class ChunkGeneratorSettings : SubsystemSettings<ChunkGeneratorSubsystem>
    {
        public GenerationSettings settings;
    }
}