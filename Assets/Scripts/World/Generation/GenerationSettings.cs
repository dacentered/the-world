﻿using System;
using System.Collections.Generic;

namespace World.Generation
{
    [Serializable]
    public class GenerationSettings
    {
        public List<NoiseOctaveSettings> HeightmapNoiseSettings;
        public List<NoiseOctaveSettings> MainNoiseSettings;
    }
}