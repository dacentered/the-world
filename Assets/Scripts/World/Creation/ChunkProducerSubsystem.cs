﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Core;
using Core.JobSystem;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Profiling;
using World.ChunkPipeline;
using World.Structure;
using SubsystemManager = Core.SubsystemManager;

namespace World.Creation
{
    public class ChunkProducerSubsystem : UpdateableSubsystem
    {
        internal ChunkPool chunkPool;
        public ChunkProducerSettings parameters;
        private string SavePath;

        public Queue<Chunk> ToRemove;
        public Structure.World world;

        public override void Initialize(SubsystemSettings settings)
        {
            base.Initialize(settings);
            this.parameters = settings as ChunkProducerSettings;
            this.chunkPool = new ChunkPool(this.parameters.ChunkPrefab);
            this.ToRemove = new Queue<Chunk>();
        }

        public override void Run()
        {
            base.Run();
            this.world = Object.FindObjectOfType<Structure.World>();
        }

        public void InitWorld(Structure.World world, int area)
        {
            world.Chunks = new ConcurrentDictionary<int3, Chunk>();
            world.ChunkPositions =
                new NativeHashSet<int3>((2 * area + 1) * (2 * area + 1) * (2 * area + 1), Allocator.Persistent);
            world.ChunkSize = 16;
        }

        public override void Stop()
        {
            base.Stop();
            this.world.ChunkPositions.Dispose();
        }

        public void Pregenerate(Structure.World world, int area)
        {
            for (int y = -area / 2; y < area / 2; ++y)
                for (int x = -area; x < area; ++x)
                    for (int z = -area; z < area; ++z)
                    {   
                        int3 coords = new int3(x, y, z);
                        world.ChunkPositions.Add(coords);
                        SubsystemManager.Get<JobSystem>().Unity.AddJob(
                            new CreateChunkJob(coords, world, this.chunkPool));
                    }
        }

        public override void Tick()
        {
            if (!this.world.Pregenerated)
                return;

            JobSystem jobSystem = SubsystemManager.Get<JobSystem>();
            int3 pos = (int3) ((float3) Camera.main.transform.position / this.world.ChunkSize);

            this.FindChunksToRemove(pos);
            this.RemoveChunks(jobSystem);
            this.CreateChunks(pos, jobSystem);
        }

        private void RemoveChunks(JobSystem jobSystem)
        {
            while (this.ToRemove.Count > 0)
            {
                Chunk chunk = this.ToRemove.Dequeue();
                Profiler.BeginSample("RemovingPosition");
                if (this.world.ChunkPositions.Contains(chunk.Position))
                    this.world.ChunkPositions.Remove(chunk.Position);
                if (this.world.Chunks.ContainsKey(chunk.Position)) this.world.Chunks.TryRemove(chunk.Position, out _);
                Profiler.EndSample();
                SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().SetState<DestoyingChunkState>(chunk);
                jobSystem.Unity.AddJob(new DestroyChunkJob(chunk, this.world, this.chunkPool));
            }
        }

        private void FindChunksToRemove(int3 pos)
        {
            foreach (Chunk chunk in this.world.Chunks.Select(ch => ch.Value)
                .Where(ch => this.IsOutRange(ch.Position, pos))) this.ToRemove.Enqueue(chunk);
        }

        // ReSharper disable once WarningHighlighting
        private void CreateChunks(int3 pos, JobSystem jobSystem)
        {
            for (int y = this.parameters.VisionSize/ 2; y >= -this.parameters.VisionSize / 2; --y)
                for (int x = -this.parameters.VisionSize; x < this.parameters.VisionSize; ++x)
                    for (int z = -this.parameters.VisionSize; z < this.parameters.VisionSize; ++z)
                        if (this.ShouldCreateChunk(pos, x, y, z))
                        {
                            this.CreateChunk(pos, jobSystem, x, y, z);
                        }
        }

        private void CreateChunk(int3 pos, JobSystem jobSystem, int x, int y, int z)
        {
            int3 coords = pos + new int3(x, y, z);
            this.world.ChunkPositions.Add(coords);
            jobSystem.Unity.AddJob(new CreateChunkJob(coords, this.world, this.chunkPool));
        }

        private bool ShouldCreateChunk(int3 pos, int x, int y, int z) => !this.world.ChunkPositions.Contains(pos + new int3(x, y, z));

        public void LoadChunk(int3 pos)
        {
            this.world.ChunkPositions.Add(pos);
            SubsystemManager.Get<JobSystem>().Unity.AddJob(new CreateChunkJob(pos, this.world, this.chunkPool));
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsOutRange(int3 position, int3 pos)
        {
            int3 dist = position - pos;
            return dist.x > this.parameters.VisionSize || dist.y > this.parameters.VisionSize ||
                   dist.z > this.parameters.VisionSize || dist.x < -this.parameters.VisionSize ||
                   dist.y < -this.parameters.VisionSize || dist.z < -this.parameters.VisionSize;
        }
    }
}