﻿using Core.JobSystem;
using UnityEngine.Profiling;
using World.Structure;

namespace World.Creation
{
    public struct DestroyChunkJob : IJob
    {
        private readonly Chunk chunk;
        private readonly Structure.World world;
        private readonly ChunkPool chunkPool;

        public DestroyChunkJob(Chunk chunk, Structure.World world, ChunkPool chunkPool)
        {
            this.chunk = chunk;
            this.world = world;
            this.chunkPool = chunkPool;
        }

        public void Execute()
        {
            if (this.chunk == null)
                return;


            Profiler.BeginSample("Deactivating");
            this.chunkPool.Return(this.chunk);
            Profiler.EndSample();
        }
    }
}