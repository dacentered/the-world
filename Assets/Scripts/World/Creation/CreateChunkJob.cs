﻿using Core;
using Core.JobSystem;
using Unity.Mathematics;
using World.ChunkPipeline;
using World.Structure;

namespace World.Creation
{
    public struct CreateChunkJob : IJob
    {
        private readonly int3 coords;
        private readonly Structure.World world;
        private readonly ChunkPool chunkPool;

        public CreateChunkJob(int3 coords, Structure.World world, ChunkPool chunkPool)
        {
            this.coords = coords;
            this.world = world;
            this.chunkPool = chunkPool;
        }

        public void Execute()
        {
            Chunk chunk = this.chunkPool.Instantiate();
            chunk.gameObject.SetActive(true);
            chunk.transform.position = (float3) this.coords * this.world.ChunkSize;
            chunk.transform.SetParent(this.world.transform);
            chunk.Size = this.world.ChunkSize;
            chunk.Position = this.coords;
            chunk.name = $"Chunk {(object) this.coords.x}:{(object) this.coords.y}:{(object) this.coords.z}";
            chunk.world = this.world;
            this.world.Chunks.TryAdd(chunk.Position, chunk);
            SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().SetState(chunk, new EmptyChunkState());
        }
    }
}