﻿using Core;
using UnityEngine;

namespace World.Creation
{
    [CreateAssetMenu]
    public class ChunkProducerSettings : SubsystemSettings<ChunkProducerSubsystem>
    {
        public GameObject ChunkPrefab;
        public int ChunkSize;
        public int VisionSize;
    }
}