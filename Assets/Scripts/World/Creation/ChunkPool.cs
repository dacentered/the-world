﻿using UnityEngine;
using UnityEngine.Profiling;
using Utils;
using World.Structure;

namespace World.Creation
{
    public class ChunkPool : ObjectPool<Chunk>
    {
        public ChunkPool(GameObject chunkPrefab) => this.ChunkPrefab = chunkPrefab;

        private GameObject ChunkPrefab { get; }

        protected override Chunk Create() => Object.Instantiate(this.ChunkPrefab).GetComponent<Chunk>();

        protected override void Clear(Chunk obj)
        {
            Profiler.BeginSample("Deactivating gameobject");
            obj.gameObject.SetActive(false);
            Profiler.EndSample();
        }
    }
}