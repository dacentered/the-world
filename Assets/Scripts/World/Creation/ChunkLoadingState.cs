﻿namespace World.Creation
{
    public enum ChunkLoadingState
    {
        Loaded,
        Unloading,
        Unloaded
    }
}