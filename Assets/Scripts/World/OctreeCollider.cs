﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Core.Memory;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Profiling;

namespace Utils
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct OctreeCollider : IDisposable
    {
        public int Count { get; private set; }
        private bool splitted;

        private readonly int size;
        private readonly float3 center;

        private OctreeChildArray* children;

        private readonly byte* startPointer;
        private readonly long length;


        public OctreeCollider(byte* parentStart, long parentLength, OctreeLeaf type, float3 parentCenter,
            int parentSize)
        {
            Profiler.BeginSample("Creating node");
            this.startPointer = parentStart + (byte) type * parentLength / 8;
            this.length = parentLength / 8;

            this.splitted = false;
            this.Count = 0;
            this.children = default;
            float childSize = parentSize / 4f;
            switch (type)
            {
                case OctreeLeaf.FUL:
                    this.center = parentCenter + new float3(-childSize, childSize, -childSize);
                    break;
                case OctreeLeaf.FUR:
                    this.center = parentCenter + new float3(childSize, childSize, -childSize);
                    break;
                case OctreeLeaf.FDL:
                    this.center = parentCenter + new float3(-childSize, -childSize, -childSize);
                    break;
                case OctreeLeaf.FDR:
                    this.center = parentCenter + new float3(childSize, -childSize, -childSize);
                    break;
                case OctreeLeaf.BUL:
                    this.center = parentCenter + new float3(-childSize, childSize, childSize);
                    break;
                case OctreeLeaf.BUR:
                    this.center = parentCenter + new float3(childSize, childSize, childSize);
                    break;
                case OctreeLeaf.BDL:
                    this.center = parentCenter + new float3(-childSize, -childSize, childSize);
                    break;
                case OctreeLeaf.BDR:
                    this.center = parentCenter + new float3(childSize, -childSize, childSize);
                    break;
                default:
                    this.center = default;
                    break;
            }

            this.size = parentSize / 2;

            Profiler.EndSample();
        }

        public OctreeCollider(int octreeSize, float3 center, int size)
        {
            Profiler.BeginSample("Creating node");
            this.splitted = false;
            this.startPointer = (byte*) MemorySubsystem.Allocate<byte>(
                octreeSize * octreeSize * octreeSize);
            this.length = octreeSize * octreeSize * octreeSize * UnsafeUtility.SizeOf<byte>();
            this.Count = 0;
            this.center = center;
            this.size = size;
            this.children = default;
            Profiler.EndSample();
        }

        public void Add(float3 position, byte value)
        {
            this.Count++;
            ref OctreeCollider child = ref this;
            while (child.size != 1)
            {
                if (!child.splitted) child.Split();
                Profiler.BeginSample("Finding node");
                child = ref child.Find(position);
                Profiler.EndSample();
                child.Count++;
            }

            *child.startPointer = value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private ref OctreeCollider Find(float3 position) => ref this.children->Get(this.GetChildId(position));

        // ReSharper disable once WarningHighlighting
        public int GetChildId(float3 position)
        {
            bool up = position.y > this.center.y;
            bool front = position.z < this.center.z;
            bool left = position.x < this.center.x;


            if (up && front && left)
                //FUL
                return 0;
            if (up && front)
                return 1;
            //FUR
            if (!up && front && left)
                return 2;
            //FDL
            if (!up && front)
                return 3;
            //FDR
            if (up && !front && left)
                return 4;
            //BUL
            if (up && !front)
                return 5;
            //BUR
            if (!up && !front && left)
                return 6;
            //BDL
            if (!up && !front)
                return 7;
            //BDR
            return -1;
        }

        private void Split()
        {
            this.splitted = true;
            Profiler.BeginSample("Creating children array");
            this.children = (OctreeChildArray*) ChunkChildrenArrayPool.Get();
            Profiler.EndSample();
            for (int i = 0; i < 8; i++)
            {
                ref OctreeCollider octreeCollider = ref this.children->Get(i);
                octreeCollider = new OctreeCollider(this.startPointer, this.length,
                    (OctreeLeaf) i, this.center,
                    this.size);
            }
        }


        public void Optimize()
        {
            if (this.splitted)
            {
                if (this.Count == 0 || this.Count == this.length)
                    this.UnSplit();
                else
                    for (int i = 0; i < 8; i++)
                        this.children->Get(i).Optimize();
            }

            /*else
            {
                if (this.Count > 0 && this.blocks.Length != 1&&this.Count!=this.blocks.Length)
                {
                    this.Splitted = true;
                    this.Split();
                    foreach (OctreeCollider octreeCollider in this.children)
                    {
                        octreeCollider.Optimize();
                    }
                }
            }*/
        }

        private void UnSplit()
        {
            this.splitted = false;
            ChunkChildrenArrayPool.Return((long) this.children);
            this.children = null;
        }


        public List<BoxCollider> Build(ColliderPool pool)
        {
            var colliders = new List<BoxCollider>();
            if (this.splitted)
            {
                for (int i = 0; i < 8; i++) colliders.AddRange(this.children->Get(i).Build(pool));
            }
            else
            {
                if (this.Count != 0)
                {
                    BoxCollider collider = pool.Instantiate();

                    collider.center = this.center;
                    collider.size = new float3(this.size, this.size, this.size);
                    colliders.Add(collider);
                }
            }

            return colliders;
        }

        [Flags]
        public enum OctreeLeaf : byte
        {
            FUL,
            FUR,
            FDL,
            FDR,
            BUL,
            BUR,
            BDL,
            BDR,
            Parent
        }


        public void Dispose()
        {
            UnsafeUtility.Free(this.startPointer, Allocator.Persistent);

            this.DisposeChild();
        }

        private void DisposeChild()
        {
            if (this.splitted)
            {
                for (int i = 0; i < 8; i++)
                {
                    ref OctreeCollider octreeCollider = ref this.children->Get(i);
                    octreeCollider.DisposeChild();
                }

                ChunkChildrenArrayPool.Return((long) this.children);
                this.children = null;
                this.splitted = false;
            }
        }
    }


    internal class OctreeChildArrayViewer
    {
        public OctreeCollider[] colliders;

        public OctreeChildArrayViewer(OctreeChildArray array)
        {
            this.colliders = new OctreeCollider[8];

            for (int i = 0; i < 8; i++) this.colliders[i] = array[i];
        }
    }
}