﻿using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using Utils;

namespace World.Rendering
{
    public class MeshDataPool : ObjectPool<MeshData>
    {
        private readonly List<MeshData> allAllocations;
        private bool Disposed;

        public MeshDataPool() => this.allAllocations = new List<MeshData>();

        public void Dispose()
        {
            this.Disposed = true;
            foreach (MeshData allAllocation in this.allAllocations)
                allAllocation.Dispose();
        }

        protected override MeshData Create()
        {
            MeshData meshData = new MeshData
            {
                verts = new NativeList<Vector3>(Allocator.Persistent),
                uvs = new NativeList<Vector2>(Allocator.Persistent),
                tris = new NativeList<int>(Allocator.Persistent),
                normals = new NativeList<Vector3>(Allocator.Persistent)
            };

            return meshData;
        }

        protected override void Clear(MeshData obj)
        {
            if (!this.Disposed)
            {
                obj.verts.Clear();
                obj.uvs.Clear();
                obj.tris.Clear();
                obj.normals.Clear();
                this.allAllocations.Add(obj);
            }
            else
            {
                obj.Dispose();
            }
        }
    }
}