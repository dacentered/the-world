﻿using System;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;

namespace World.Rendering
{
    public struct MeshData : IDisposable
    {
        public NativeList<Vector3> verts;
        public NativeList<int> tris;
        public NativeList<Vector2> uvs;
        public NativeList<Vector3> normals;

        public void Dispose()
        {
            this.verts.Dispose();
            this.tris.Dispose();
            this.uvs.Dispose();
            this.normals.Dispose();
        }

        public void AssignToMeshAndReturn(Mesh visualMesh, MeshDataPool pool)
        {
            visualMesh.SetVertices(this.verts.AsArray());
            visualMesh.indexFormat = IndexFormat.UInt32;
            visualMesh.SetIndices(this.tris.AsArray(), MeshTopology.Triangles, 0);
            visualMesh.SetUVs(0, this.uvs.AsArray());
            visualMesh.SetNormals(this.normals.AsArray());
            Profiler.BeginSample("Returning to pool");
            pool.Return(this);
            Profiler.EndSample();
        }
    }
}