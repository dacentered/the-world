﻿using Core;
using UnityEngine;
using World.Structure;
using Subsystem = Core.Subsystem;

namespace World.Rendering
{
    public class ChunkRendererSubsystem : Subsystem
    {
        public Texture2D chunkTexture;


        public override void Initialize(SubsystemSettings settings)
        {
            base.Initialize(settings);
            Application.targetFrameRate = 60;
            this.chunkTexture = BlockInfo.BuildTexture();
        }
    }
}