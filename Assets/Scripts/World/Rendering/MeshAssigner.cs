﻿using UnityEngine;
using UnityEngine.Profiling;
using Utils;
using World.ChunkPipeline;
using World.Structure;
using SubsystemManager = Core.SubsystemManager;

namespace World.Rendering
{
    public class MeshAssigner : ChunkProcessor
    {
        private readonly MeshDataPool meshDataPool;
        public MeshPool meshPool;

        public MeshAssigner()
        {
            this.Multithreaded = false;
            this.meshPool = new MeshPool();
            this.meshPool.Pregenerate(10000);
            this.meshDataPool = SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().GetProcessor<ChunkRenderer>()
                .meshDataPool;
        }

        public override void Process(Chunk chunk)
        {
            MeshData meshData =
                SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().GetProcessor<ChunkRenderer>()[chunk];
            if (chunk == null ||
                SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().GetState(chunk) is DestoyingChunkState)
            {
                this.meshDataPool.Return(meshData);
            }
            else
            {
                Profiler.BeginSample("GetMesh");
                Mesh visualMesh = this.meshPool.Instantiate();
                Profiler.EndSample();
                Profiler.BeginSample("SetMeshData");
                meshData.AssignToMeshAndReturn(visualMesh, this.meshDataPool);
                Profiler.EndSample();
                Profiler.BeginSample("Assign mesh");
                chunk.Mesh = visualMesh;
                chunk.MeshFilter.mesh = visualMesh;

                chunk.MeshRenderer.material.mainTexture =
                    SubsystemManager.Get<ChunkRendererSubsystem>().chunkTexture;
                Profiler.EndSample();
            }
        }
    }
}