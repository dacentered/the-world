﻿using System;
using Core;
using Utils;
using World.ChunkPipeline;
using World.Structure;

namespace World.Rendering
{
    public class VisibilityBuilder : ChunkProcessorWithResult<FlattenedArray<VisibilityBuilder.Transparent>>
    {
        [Flags]
        public enum Transparent : byte
        {
            Left = 1,
            Right = 2,
            Top = 4,
            Bottom = 8,
            Front = 16, // 0x10
            Back = 32, // 0x20
            This = 64 // 0x40
        }

        public VisibilityBuilder() => this.Multithreaded = true;

        public override void Process(Chunk chunk)
        {
            
            if (chunk == null ||
                SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().GetState(chunk) is DestoyingChunkState)
                return;
            var result = new FlattenedArray<Transparent>(chunk.Size + 2, chunk.Size + 2,
                chunk.Size + 2);
            FillOpaqueMap(chunk,ref result);

            FillTransparencyMap(chunk,ref result);

            this[chunk] = result;
        }

        private static void FillTransparencyMap(Chunk chunk, ref FlattenedArray<Transparent> result)
        {
            for (int x = 0; x < chunk.blocks.XSize; ++x)
                for (int y = 0; y < chunk.blocks.YSize; ++y)
                    for (int z = 0; z < chunk.blocks.ZSize; ++z)
                        CalculateTransparency(ref result, x, y, z);

        }

        private static void CalculateTransparency(ref FlattenedArray<Transparent> result, int x, int y, int z)
        {
            if ((result[x - 1 + 1, y + 1, z + 1] & Transparent.This) == Transparent.This)
                result[x + 1, y + 1, z + 1] =
                    result[x + 1, y + 1, z + 1] | Transparent.Left;
            if ((result[x + 1 + 1, y + 1, z + 1] & Transparent.This) == Transparent.This)
                result[x + 1, y + 1, z + 1] =
                    result[x + 1, y + 1, z + 1] | Transparent.Right;
            if ((result[x + 1, y - 1 + 1, z + 1] & Transparent.This) == Transparent.This)
                result[x + 1, y + 1, z + 1] =
                    result[x + 1, y + 1, z + 1] | Transparent.Bottom;
            if ((result[x + 1, y + 1 + 1, z + 1] & Transparent.This) == Transparent.This)
                result[x + 1, y + 1, z + 1] =
                    result[x + 1, y + 1, z + 1] | Transparent.Top;
            if ((result[x + 1, y + 1, z - 1 + 1] & Transparent.This) == Transparent.This)
                result[x + 1, y + 1, z + 1] =
                    result[x + 1, y + 1, z + 1] | Transparent.Back;
            if ((result[x + 1, y + 1, z + 1 + 1] & Transparent.This) == Transparent.This)
                result[x + 1, y + 1, z + 1] =
                    result[x + 1, y + 1, z + 1] | Transparent.Front;
        }

        private static void FillOpaqueMap(Chunk chunk, ref FlattenedArray<Transparent> result)
        {
            for (int x = -1; x < chunk.blocks.XSize + 1; ++x)
                for (int y = -1; y < chunk.blocks.YSize + 1; ++y)
                    for (int z = -1; z < chunk.blocks.ZSize + 1; ++z)
                        result[x + 1, y + 1, z + 1] = IsTransparent(chunk, x, y, z) ? Transparent.This : 0;
        }

        private static bool IsTransparent(Chunk chunk, int x, int y, int z)
        {
            if (x >= chunk.Size || y >= chunk.Size || z >= chunk.Size || x < 0 || y < 0 || z < 0)
                return true;
            return BlockInfo.GetBlockInfoForBlock(chunk.blocks[x, y, z].Id).Transparent;
        }
    }
}