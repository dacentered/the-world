﻿namespace World.Rendering
{
    /*public struct BuildOctreeJob : IJob
    {
        private readonly Chunk chunk;
        private readonly ColliderPool colliderPool;

        public BuildOctreeJob(Chunk chunk, ColliderPool colliderPool)
        {
            this.chunk = chunk;
            this.colliderPool = colliderPool;
        }

        public void Execute()
        {
            return;
            if (this.chunk == null || this.chunk.ChunkLoadingState != ChunkLoadingState.Loaded ||
                this.chunk.ChunkState is DestoyingChunkState)
                return;
            float3 float3 = new float3(this.chunk.Size, this.chunk.Size, this.chunk.Size);
            Profiler.BeginSample("Creating octree");
            this.chunk.Collider = new OctreeCollider(this.chunk.Size,
                new float3(this.chunk.Size / 2, this.chunk.Size / 2, this.chunk.Size / 2), this.chunk.Size);
            Profiler.EndSample();
            Profiler.BeginSample("Filling octree");
            for (int x = 0; x < this.chunk.blocks.XSize; ++x)
            for (int y = 0; y < this.chunk.blocks.YSize; ++y)
            for (int z = 0; z < this.chunk.blocks.ZSize; ++z)
                if (this.chunk.blocks[x, y, z].Id > 0)
                    this.chunk.Collider.Add(new float3(x + 0.5f, y + 0.5f, z + 0.5f), 1);
            Profiler.EndSample();
            Profiler.BeginSample("Optimizing octree");
            this.chunk.Collider.Optimize();
            Profiler.EndSample();
            //if (this.chunk.Collider.Count > 0)
            //    SubsystemManager.Get<JobSystem>()
            //        .Unity.AddJob(SubsystemManager.Get<ChunkRendererSubsystem>(),new AssignCollisionJob(this.chunk, this.colliderPool));
        }
    }*/
}