﻿using UnityEngine;
using UnityEngine.Profiling;
using World.ChunkPipeline;
using World.Structure;
using SubsystemManager = Core.SubsystemManager;

namespace World.Rendering
{
    public class ChunkRenderCleaner : ChunkProcessor
    {
        public override void Process(Chunk chunk)
        {
            Debug.Log("Cleaning chunk");
            Profiler.BeginSample("MeshReturning");
            SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().GetProcessor<MeshAssigner>().meshPool
                .Return(chunk.Mesh);
            //this.ReturnColliders(chunk.GetComponentsInChildren<BoxCollider>());
            Profiler.EndSample();
            Profiler.BeginSample("Nulling reference");
            chunk.MeshFilter.mesh = null;
            chunk.Mesh = null;
            Profiler.EndSample();
        }
    }
}