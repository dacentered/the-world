﻿using System.Runtime.CompilerServices;

namespace World.Rendering
{
    public static class EnumExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool Has(this VisibilityBuilder.Transparent e, VisibilityBuilder.Transparent flag) =>
            (uint) (e & flag) > 0U;
    }
}