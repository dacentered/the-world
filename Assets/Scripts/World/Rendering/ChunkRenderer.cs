﻿using Core;
using Unity.Collections;
using Unity.Mathematics;
using Utils;
using World.ChunkPipeline;
using World.Structure;

namespace World.Rendering
{
    public class ChunkRenderer : ChunkProcessorWithResult<MeshData>
    {
        public MeshDataPool meshDataPool;

        public ChunkRenderer()
        {
            this.meshDataPool = new MeshDataPool();
            this.meshDataPool.Pregenerate(4096);
            this.Multithreaded = true;
        }

        public override void Process(Chunk chunk)
        {
            if (chunk == null ||
                SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().GetState(chunk) is DestoyingChunkState)
                return;

            var vision =
                SubsystemManager.Get<ChunkPipeline.ChunkPipeline>().GetProcessor<VisibilityBuilder>()[chunk];


            MeshData meshData = this.meshDataPool.Instantiate();

            var faces = new NativeList<BlockFace>(Allocator.Persistent);
            BuildBlockFaces(chunk,ref faces,in vision);
            vision.Dispose();
            foreach (BlockFace t in faces) BuildFace(t, ref meshData);

            faces.Dispose();
            this[chunk] = meshData;
        }

        private static void BuildBlockFaces(Chunk chunk,ref NativeList<BlockFace> faces,in  FlattenedArray<VisibilityBuilder.Transparent> vision)
        {
            for (int x = 0; x < chunk.blocks.XSize; ++x)
                for (int y = 0; y < chunk.blocks.YSize; ++y)
                    for (int z = 0; z < chunk.blocks.ZSize; ++z)
                        GetBlockFaces(chunk.blocks[x, y, z], x, y, z, faces, in vision);
        }

        private static void GetBlockFaces(Block block, int x, int y, int z, NativeList<BlockFace> faces,
            in FlattenedArray<VisibilityBuilder.Transparent> vision)
        {
            if ( /*!vision[x + 1, y + 1, z + 1].Has(VisibilityBuilder.Transparent.This)||*/block.Id == 0) return;
            if (vision[x + 1, y + 1, z + 1].Has(VisibilityBuilder.Transparent.Left))
                faces.Add(BlockFace.Left.AtPos(new float3(x, y, z)).WithBlock(block));
            if (vision[x + 1, y + 1, z + 1].Has(VisibilityBuilder.Transparent.Right))
                faces.Add(BlockFace.Right.AtPos(new float3(x + 1, y, z)).WithBlock(block));
            if (vision[x + 1, y + 1, z + 1].Has(VisibilityBuilder.Transparent.Bottom))
                faces.Add(BlockFace.Bottom.AtPos(new float3(x, y, z)).WithBlock(block));
            if (vision[x + 1, y + 1, z + 1].Has(VisibilityBuilder.Transparent.Top))
                faces.Add(BlockFace.Top.AtPos(new float3(x, y + 1, z)).WithBlock(block));
            if (vision[x + 1, y + 1, z + 1].Has(VisibilityBuilder.Transparent.Back))
                faces.Add(BlockFace.Back.AtPos(new float3(x, y, z)).WithBlock(block));
            if (vision[x + 1, y + 1, z + 1].Has(VisibilityBuilder.Transparent.Front))
                faces.Add(BlockFace.Front.AtPos(new float3(x, y, z + 1)).WithBlock(block));
        }

        private static void BuildFace(
            BlockFace face,
            ref MeshData meshData)
        {
            
            AddVertices(face,ref meshData);
            AddUVs(face,ref meshData);
            AddIndices(face,ref meshData);
            AddNormals(face, ref meshData);
        }

        private static void AddIndices(BlockFace face,ref MeshData meshData)
        {
            int length = meshData.verts.Length;
            if (face.Reversed)
            {
                meshData.tris.Add(length);
                meshData.tris.Add(length + 1);
                meshData.tris.Add(length + 2);
                meshData.tris.Add(length + 2);
                meshData.tris.Add(length + 3);
                meshData.tris.Add(length);
            }
            else
            {
                meshData.tris.Add(length + 1);
                meshData.tris.Add(length);
                meshData.tris.Add(length + 2);
                meshData.tris.Add(length + 3);
                meshData.tris.Add(length + 2);
                meshData.tris.Add(length);
            }
        }

        private static void AddNormals(BlockFace face, ref MeshData meshData)
        {
            if (face.Reversed)
            {
                meshData.normals.Add(math.cross(face.UpVec, face.RightVec));
                meshData.normals.Add(math.cross(face.UpVec, face.RightVec));
                meshData.normals.Add(math.cross(face.UpVec, face.RightVec));
                meshData.normals.Add(math.cross(face.UpVec, face.RightVec));
            }
            else
            {

                meshData.normals.Add(math.cross(face.RightVec, face.UpVec));
                meshData.normals.Add(math.cross(face.RightVec, face.UpVec));
                meshData.normals.Add(math.cross(face.RightVec, face.UpVec));
                meshData.normals.Add(math.cross(face.RightVec, face.UpVec));
            }
        }

        private static void AddVertices(BlockFace face,ref MeshData meshData)
        {
            meshData.verts.Add(face.PosVec);
            meshData.verts.Add(face.PosVec + face.UpVec);
            meshData.verts.Add(face.PosVec + face.UpVec + face.RightVec);
            meshData.verts.Add(face.PosVec + face.RightVec);
        }

        private static void AddUVs(BlockFace face,ref MeshData meshData)
        {
            var uvsForBlock = BlockInfo.GetBlockInfoForBlock(face.Block.Id).UVs;

            meshData.uvs.Add(uvsForBlock[0]);
            meshData.uvs.Add(uvsForBlock[1]);
            meshData.uvs.Add(uvsForBlock[2]);
            meshData.uvs.Add(uvsForBlock[3]);
        }

        public struct BlockFace
        {
            public float3 RightVec;

            public float3 UpVec;

            public float3 PosVec;

            public bool Reversed;

            public Block Block;

            public static BlockFace Top = new BlockFace
                {UpVec = math.forward(), RightVec = math.right(), Reversed = true};

            public static BlockFace Bottom = new BlockFace
                {UpVec = math.forward(), RightVec = math.right(), Reversed = false};

            public static BlockFace Left = new BlockFace
                {UpVec = math.up(), RightVec = math.forward(), Reversed = false};

            public static BlockFace Right = new BlockFace
                {UpVec = math.up(), RightVec = math.forward(), Reversed = true};

            public static BlockFace Front = new BlockFace
                {UpVec = math.up(), RightVec = math.right(), Reversed = false};

            public static BlockFace Back = new BlockFace {UpVec = math.up(), RightVec = math.right(), Reversed = true};

            public BlockFace AtPos(float3 pos) => new BlockFace
                {PosVec = pos, RightVec = this.RightVec, Reversed = this.Reversed, UpVec = this.UpVec};

            public BlockFace WithBlock(Block block) =>
                new BlockFace
                {
                    PosVec = this.PosVec, RightVec = this.RightVec, Reversed = this.Reversed, UpVec = this.UpVec,
                    Block = block
                };
        }
    }
}