﻿using System.Diagnostics;
using System.Runtime.InteropServices;
using Unity.Collections.LowLevel.Unsafe;

namespace Utils
{
    [StructLayout(LayoutKind.Sequential)]
    [DebuggerTypeProxy(typeof(OctreeChildArrayViewer))]
    public unsafe struct OctreeChildArray
    {
        public void* Memory;


        public OctreeChildArray(bool pointer) => this.Memory = (void*) ChunkChildrenPool.Get();


        public ref OctreeCollider this[int index] =>
            ref UnsafeUtility.ArrayElementAsRef<OctreeCollider>(this.Memory, index);


        public ref OctreeCollider Get(int index) =>
            ref UnsafeUtility.ArrayElementAsRef<OctreeCollider>(this.Memory, index);


        public void Release() => ChunkChildrenPool.Return((long) this.Memory);
    }
}