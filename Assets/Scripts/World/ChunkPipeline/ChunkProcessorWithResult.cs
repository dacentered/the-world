﻿using System.Collections.Concurrent;
using Unity.Mathematics;
using World.Structure;

namespace World.ChunkPipeline
{
    public abstract class ChunkProcessorWithResult<T> : ChunkProcessor
    {
        private readonly ConcurrentDictionary<int3, T> Results;

        protected ChunkProcessorWithResult() =>
            this.Results = new ConcurrentDictionary<int3, T>();

        public T this[Chunk chunk]
        {
            get => this.Results[chunk.Position];
            set => this.Results[chunk.Position] = value;
        }
    }
}