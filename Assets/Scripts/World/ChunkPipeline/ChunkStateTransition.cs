﻿namespace World.ChunkPipeline
{
    public struct ChunkStateTransition
    {
        public ChunkState FromState;

        public ChunkState ToState;

        public ChunkProcessor Processor;

        public ChunkStateTransition(ChunkState from, ChunkState to, ChunkProcessor processor)
        {
            this.FromState = from;
            this.ToState = to;
            this.Processor = processor;
        }

        public static ChunkStateTransition From<T>() where T : ChunkState, new() =>
            new ChunkStateTransition {FromState = new T()};

        public static ChunkStateTransition From(ChunkState chunkState) =>
            new ChunkStateTransition {FromState = chunkState};

        public ChunkStateTransition To<T>() where T : ChunkState, new() => new ChunkStateTransition
            {FromState = this.FromState, ToState = new T()};

        public ChunkStateTransition WithProcessor(ChunkProcessor processor) => new ChunkStateTransition
            {FromState = this.FromState, ToState = this.ToState, Processor = processor};

        public ChunkStateTransition WithProcessor<T>() where T : ChunkProcessor, new() => new ChunkStateTransition
            {FromState = this.FromState, ToState = this.ToState, Processor = new T()};
    }
}