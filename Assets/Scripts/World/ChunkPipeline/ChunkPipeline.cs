﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Core;
using Core.JobSystem;
using World.Structure;

namespace World.ChunkPipeline
{
    [AutoStart]
    public class ChunkPipeline : Subsystem
    {
        private readonly List<ChunkProcessor> processors = new List<ChunkProcessor>();
        private readonly List<ChunkStateTransition> transitions = new List<ChunkStateTransition>();


        private readonly Dictionary<Chunk, ChunkState> states = new Dictionary<Chunk, ChunkState>();
        public T GetProcessor<T>() where T : ChunkProcessor => this.processors.Find(p => p is T) as T;

        private void ChunkStateChanged(ChunkState oldState, ChunkState newState, Chunk chunk)
        {
            foreach (ChunkStateTransition chunkStateTransition in this.transitions
                .Where(
                    tr => (tr.FromState == null
                           || tr.FromState?.GetType() == oldState?.GetType())
                          && tr.ToState.GetType() == newState.GetType()))
                this.RunProcess(chunk, chunkStateTransition);
        }

        private void RunProcess(Chunk chunk, ChunkStateTransition chunkStateTransition)
        {
            if (chunkStateTransition.Processor.Multithreaded)
                this.RunProcess(chunk, chunkStateTransition, SubsystemManager.Get<JobSystem>().Threaded);
            else
                this.RunProcess(chunk, chunkStateTransition, SubsystemManager.Get<JobSystem>().Unity);
        }

        private void RunProcess(Chunk chunk, ChunkStateTransition chunkStateTransition, IJobExecutor executor)
        {
            this.SetState(chunk,
                (ChunkState) Activator.CreateInstance(
                    typeof(ProcessingChunkState<>)
                        .MakeGenericType(chunkStateTransition.Processor.GetType()
                        )));
            Interlocked.Increment(ref chunkStateTransition.Processor.ProcessingCount);
            executor
                .AddJob(new ProcessChunkJob(chunk, chunkStateTransition.Processor)).OnEnd += _ => SubsystemManager.Get<ChunkPipeline>().SetState(chunk,
                    (ChunkState)Activator.CreateInstance(
                        typeof(EndedProcessChunkState<>)
                            .MakeGenericType(chunkStateTransition.Processor.GetType()
                            )));
        }


        public void RegisterProcess(ChunkStateTransition transition)
        {
            this.transitions.Add(transition);
            if (!this.processors.Exists(s => s.GetType() == transition.Processor.GetType()))
                this.processors.Add(transition.Processor);
        }

        public void SetState<T>(Chunk chunk) where T : ChunkState, new()
        {
            this.states.TryGetValue(chunk, out ChunkState oldState);
            this.states[chunk] = new T();
            this.ChunkStateChanged(oldState, new T(), chunk);
        }

        public void SetState(Chunk chunk, ChunkState state)
        {
            this.states.TryGetValue(chunk, out ChunkState oldState);
            this.states[chunk] = state;
            this.ChunkStateChanged(oldState, state, chunk);
        }

        public ChunkState GetState(Chunk chunk) => this.states.TryGetValue(chunk, out ChunkState state) ? state : null;
    }
}