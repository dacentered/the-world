﻿using World.Structure;

namespace World.ChunkPipeline
{
    public abstract class ChunkProcessor
    {
        public bool Multithreaded;
        public int ProcessingCount;
        public abstract void Process(Chunk chunk);
    }
}