using Core;
using World.Generation;
using World.Rendering;

namespace World.ChunkPipeline.Default
{
    [AutoStart]
    public class DefaultChunkPipeline : Subsystem
    {
        public override void Initialize(SubsystemSettings settings)
        {
            base.Initialize(settings);

            SubsystemManager.Get<ChunkPipeline>()
                .RegisterProcess(
                    ChunkStateTransition
                        .From(null)
                        .To<EmptyChunkState>()
                        .WithProcessor<ChunkGenerator>());
            SubsystemManager.Get<ChunkPipeline>()
                .RegisterProcess(
                    ChunkStateTransition
                        .From<ProcessingChunkState<ChunkGenerator>>()
                        .To<EndedProcessChunkState<ChunkGenerator>>()
                        .WithProcessor<ChunkDecorator>());

            SubsystemManager.Get<ChunkPipeline>()
                .RegisterProcess(
                    ChunkStateTransition
                        .From<ProcessingChunkState<ChunkDecorator>>()
                        .To<EndedProcessChunkState<ChunkDecorator>>()
                        .WithProcessor<VisibilityBuilder>());
            SubsystemManager.Get<ChunkPipeline>()
                .RegisterProcess(
                    ChunkStateTransition
                        .From(null)
                        .To<ChangedChunkState>()
                        .WithProcessor<VisibilityBuilder>());

            SubsystemManager.Get<ChunkPipeline>()
                .RegisterProcess(
                    ChunkStateTransition
                        .From<ProcessingChunkState<VisibilityBuilder>>()
                        .To<EndedProcessChunkState<VisibilityBuilder>>()
                        .WithProcessor<ChunkRenderer>());

            SubsystemManager.Get<ChunkPipeline>()
                .RegisterProcess(
                    ChunkStateTransition
                        .From<ProcessingChunkState<ChunkRenderer>>()
                        .To<EndedProcessChunkState<ChunkRenderer>>()
                        .WithProcessor<MeshAssigner>());

            SubsystemManager.Get<ChunkPipeline>()
                .RegisterProcess(
                    ChunkStateTransition
                        .From<EndedProcessChunkState<MeshAssigner>>()
                        .To<DestoyingChunkState>()
                        .WithProcessor<ChunkRenderCleaner>());
        }
    }
}