﻿using System.Threading;
using Core.JobSystem;
using UnityEngine.Profiling;
using World.Structure;

namespace World.ChunkPipeline
{
    public struct ProcessChunkJob : IJob
    {
        public Chunk chunk;
        public ChunkProcessor processor;

        public ProcessChunkJob(Chunk chunk, ChunkProcessor processor)
        {
            this.chunk = chunk;
            this.processor = processor;
        }

        public void Execute()
        {
            Profiler.BeginSample(this.processor.GetType().Name);
            this.processor.Process(this.chunk);
            Profiler.EndSample();
            Interlocked.Decrement(ref this.processor.ProcessingCount);
        }
    }
}