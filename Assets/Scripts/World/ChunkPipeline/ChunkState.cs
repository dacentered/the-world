﻿namespace World.ChunkPipeline
{
    public class ChunkState
    {
    }

    public class ProcessingChunkState<T> : ChunkState where T : ChunkProcessor
    {
    }

    public class EndedProcessChunkState<T> : ChunkState where T : ChunkProcessor
    {
    }

    public class EmptyChunkState : ChunkState
    {
    }

    public class DestoyingChunkState : ChunkState
    {
    }


    public class ChangedChunkState : ChunkState
    {
    }
}