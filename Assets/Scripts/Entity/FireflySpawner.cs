﻿using UnityEngine;

namespace Entity
{
    public class FireflySpawner : EntitySpawner<Firefly>
    {
        private float accumulatedTime;
        private int count;
        public override bool IsSpawnArea(Vector3 position) => true;

        public override void Update()
        {
            this.World = Object.FindObjectOfType<World.Structure.World>();
            this.accumulatedTime += Time.deltaTime;
            if (this.count > 5) return;

            if (this.accumulatedTime > 6f)
            {
                Vector3 playerPos = Camera.main.transform.position;
                Vector3 pos = playerPos + Random.onUnitSphere * 32 + 32 * Camera.main.transform.forward;


                while (this.World.GetBlock((int) pos.x, (int) pos.y, (int) pos.z).Id != 0)
                    pos = playerPos + Random.insideUnitSphere * 32;
                this.Spawn(pos);
                this.count++;

                this.accumulatedTime = 0f;
            }
        }
    }
}