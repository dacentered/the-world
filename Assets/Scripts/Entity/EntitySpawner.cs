﻿using UnityEngine;

namespace Entity
{
    public abstract class EntitySpawner<T> : IEntitySpawner where T : Entity
    {
        public GameObject Prefab;
        protected World.Structure.World World;

        public void Initialize()
        {
            this.World = Object.FindObjectOfType<World.Structure.World>();
            this.Prefab = Resources.Load<GameObject>($"Entities\\{typeof(T).Name}");
        }

        public abstract void Update();
        public abstract bool IsSpawnArea(Vector3 position);

        protected void Spawn(Vector3 position)
        {
            if (this.IsSpawnArea(position)) Object.Instantiate(this.Prefab, position, Quaternion.identity);
        }
    }
}