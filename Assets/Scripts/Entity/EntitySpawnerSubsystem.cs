﻿using System.Collections.Generic;
using Core;

namespace Entity
{
    public class EntitySpawnerSubsystem : UpdateableSubsystem
    {
        public List<IEntitySpawner> Spawners;

        public override void Initialize(SubsystemSettings settings)
        {
            base.Initialize(settings);
            this.Spawners = new List<IEntitySpawner> {new FireflySpawner()};
            foreach (IEntitySpawner entitySpawner in this.Spawners) entitySpawner.Initialize();
        }

        public override void Tick()
        {
            foreach (IEntitySpawner entitySpawner in this.Spawners) entitySpawner.Update();
        }
    }
}