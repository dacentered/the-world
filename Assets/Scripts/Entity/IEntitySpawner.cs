﻿namespace Entity
{
    public interface IEntitySpawner
    {
        void Update();
        void Initialize();
    }
}